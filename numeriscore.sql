-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le : lun. 04 mars 2024 à 14:27
-- Version du serveur : 8.0.31
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `numeriscoretest`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(200) DEFAULT NULL,
  `Img` varchar(500) DEFAULT NULL,
  `Couleur1` varchar(100) DEFAULT NULL,
  `Couleur2` varchar(100) DEFAULT NULL,
  `Ordre` int DEFAULT NULL,
  `Description` longtext,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`Id`, `Nom`, `Img`, `Couleur1`, `Couleur2`, `Ordre`, `Description`) VALUES
('0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'Informatique', 'informatique.svg', '#816CAB', '#816CAB4D', 1, 'L\'informatique dans les collectivités territoriales est un vaste sujet. Ces dernières sont engagées dans une transformation numérique profonde : pour répondre à des obligations réglementaires et pour rendre un meilleur service aux citoyens. Le système d\'information et sa sécurité sont la clé de voute du bon fonctionnement d\'une collectivité. De la gestion des documents aux traitements des données personnelles, tout est destiné à être dématérialisé et, in fine, contenu dans le système d\'information. C\'est également par ce biais que la plupart des relations avec les citoyens est désormais effectuée.'),
('1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'Logiciel métier', 'logiciel.svg', '#863B8B', '#863B8B4D', 2, 'Regroupés au sein de trois grandes familles que sont les finances, les ressources humaines et la relation citoyen, les logiciels métiers sont incontournables en collectivités. Pour chaque famille, la règlementation est en constante évolution et les nouveaux processus de dématérialisation sont devenus monnaie courante.'),
('226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Communication', 'communication.svg', '#CC4A51', '#CC4A514D', 3, 'La communication est cruciale dans une collectivité car elle permet de maintenir une bonne cohésion sociale et de résoudre les problèmes de manière efficace : une bonne identité visuelle (pour plus de légitimité et de confiance), des outils collaboratifs (pour plus d\'efficacité), une présence sur le web accrue (pour une fiabilité des données sans faille). Dans la vie publique, la communication est particulièrement importante pour informer les citoyens, promouvoir la participation démocratique et construire des consensus sur les politiques publiques. Si la communication est claire et transparente, elle renforce la confiance dans les institutions locales, elle assure une bonne marche de la vie publique et elle favorise le développement durable d\'une collectivité.'),
('3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'Social', 'social.svg', '#787878', '#7878784D', 4, 'Le volet social n\'est pas épargné par le numérique. Il existe un bon nombre de services en lien avec la dématérialisation des services publics. Il est important dans ce domaine de prendre en compte l\'inclusion numérique au sens large.'),
('44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'Culture & loisirs', 'culture.svg', '#840016', '#8400164D', 5, 'Contrairement aux idées reçues, on retrouve dans le domaine de la culture de multiples moyens de mettre en place des outils informatiques de dématérialisation. Par exemple, ils permettent aux citoyens de faire des réservations en ligne pour des spectacles ou encore, de connaitre les horaires d\'ouverture des équipements communaux.'),
('743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'Cybersécurité', 'cybersecurite.svg', '#B16317', '#B163174D', 6, 'Face à la recrudescence des cyberattaques, la cybersécurité est un enjeu majeur pour les collectivités. En effet, la maîtrise et la sécurisation de son système d\'information est un prérequis, surtout de nos jours, lorsque l\'on veut travailler sereinement. Cela passe par la sensibilisation des équipes car l\'erreur humaine reste le vecteur principal des cyberattaques.'),
('8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 'Outil de dématérialisation', 'plateforme.svg', '#1779B1', '#1779B14D', 9, 'La dématérialisation est une utilisation efficace des technologies numériques pour remplacer les processus papier traditionnels dans les collectivités. Les outils de dématérialisation réduisent les coûts et les déchets, tout en améliorant la sécurité de l\'information.'),
('bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'Éducation & enfance', 'education.svg', '#448084', '#4480844D', 7, 'La diffusion et l\'usage des équipements numériques constituent un enjeu majeur pour faire évoluer le système éducatif en réinventant les méthodes d\'enseignement. Par ailleurs, vis-à-vis des parents, il est important de favoriser l\'accès aux services publics en proposant et en facilitant les inscriptions dans les centres périscolaires ou les cantines de leurs enfants.'),
('ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Environnement & territoire', 'environement.svg', '#45844F', '#45844F4D', 8, 'On entend beaucoup parler ces derniers temps de la sobriété numérique. L\'importance de l\'écologie ne peut plus être ignorée. Elle a un impact sur notre vie quotidienne et sur celle des générations futures. L\'objectif est de préserver la biodiversité et les écosystèmes naturels, de lutter contre les changements climatiques, de protéger la qualité de l\'air, de l\'eau et du sol, de maintenir les ressources naturelles et de garantir une qualité de vie saine pour tous les citoyens. Les collectivités peuvent jouer un rôle clé en prenant des mesures pour encourager un mode de vie durable.');

-- --------------------------------------------------------

--
-- Structure de la table `collectivite`
--

DROP TABLE IF EXISTS `collectivite`;
CREATE TABLE IF NOT EXISTS `collectivite` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(500) NOT NULL,
  `TypeId` char(36) NOT NULL,
  `Population` int NOT NULL,
  `Siret` char(14) NOT NULL,
  `DepartementCode` char(3) NOT NULL,
  `Latitude` varchar(500) NOT NULL,
  `Longitude` varchar(500) NOT NULL,
  `OPSNId` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `OPSNId` (`OPSNId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `collectivite`
--

INSERT INTO `collectivite` (`Id`, `Nom`, `TypeId`, `Population`, `Siret`, `DepartementCode`, `Latitude`, `Longitude`, `OPSNId`) VALUES
('404', 'demo', '8f9d4a91-fff3-11eb-acf0-0cc47a0ad120', 0, '44444444444444', '60', '', '', '404');

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

DROP TABLE IF EXISTS `departement`;
CREATE TABLE IF NOT EXISTS `departement` (
  `Code` char(3) NOT NULL,
  `Nom` varchar(100) NOT NULL,
  `CodeRegion` int NOT NULL,
  KEY `Code` (`Code`),
  KEY `CodeRegion` (`CodeRegion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`Code`, `Nom`, `CodeRegion`) VALUES
('01', 'Ain', 84),
('02', 'Aisne', 32),
('03', 'Allier', 84),
('04', 'Alpes-de-Haute-Provence', 93),
('05', 'Hautes-Alpes', 93),
('06', 'Alpes-Maritimes', 93),
('07', 'Ardèche', 84),
('08', 'Ardennes', 44),
('09', 'Ariège', 76),
('10', 'Aube', 44),
('11', 'Aude', 76),
('12', 'Aveyron', 76),
('13', 'Bouches-du-Rhône', 93),
('14', 'Calvados', 28),
('15', 'Cantal', 84),
('16', 'Charente', 75),
('17', 'Charente-Maritime', 75),
('18', 'Cher', 24),
('19', 'Corrèze', 75),
('21', 'Côte-d\'Or', 27),
('22', 'Côtes-d\'Armor', 53),
('23', 'Creuse', 75),
('24', 'Dordogne', 75),
('25', 'Doubs', 27),
('26', 'Drôme', 84),
('27', 'Eure', 28),
('28', 'Eure-et-Loir', 24),
('29', 'Finistère', 53),
('2A', 'Corse-du-Sud', 94),
('2B', 'Haute-Corse', 94),
('30', 'Gard', 76),
('31', 'Haute-Garonne', 76),
('32', 'Gers', 76),
('33', 'Gironde', 75),
('34', 'Hérault', 76),
('35', 'Ille-et-Vilaine', 53),
('36', 'Indre', 24),
('37', 'Indre-et-Loire', 24),
('38', 'Isère', 84),
('39', 'Jura', 27),
('40', 'Landes', 75),
('41', 'Loir-et-Cher', 24),
('42', 'Loire', 84),
('43', 'Haute-Loire', 84),
('44', 'Loire-Atlantique', 52),
('45', 'Loiret', 24),
('46', 'Lot', 76),
('47', 'Lot-et-Garonne', 75),
('48', 'Lozère', 76),
('49', 'Maine-et-Loire', 52),
('50', 'Manche', 28),
('51', 'Marne', 44),
('52', 'Haute-Marne', 44),
('53', 'Mayenne', 52),
('54', 'Meurthe-et-Moselle', 44),
('55', 'Meuse', 44),
('56', 'Morbihan', 53),
('57', 'Moselle', 44),
('58', 'Nièvre', 27),
('59', 'Nord', 32),
('60', 'Oise', 32),
('61', 'Orne', 28),
('62', 'Pas-de-Calais', 32),
('63', 'Puy-de-Dôme', 84),
('64', 'Pyrénées-Atlantiques', 75),
('65', 'Hautes-Pyrénées', 76),
('66', 'Pyrénées-Orientales', 76),
('67', 'Bas-Rhin', 44),
('68', 'Haut-Rhin', 44),
('69', 'Rhône', 84),
('70', 'Haute-Saône', 27),
('71', 'Saône-et-Loire', 27),
('72', 'Sarthe', 52),
('73', 'Savoie', 84),
('74', 'Haute-Savoie', 84),
('75', 'Paris', 11),
('76', 'Seine-Maritime', 28),
('77', 'Seine-et-Marne', 11),
('78', 'Yvelines', 11),
('79', 'Deux-Sèvres', 75),
('80', 'Somme', 32),
('81', 'Tarn', 76),
('82', 'Tarn-et-Garonne', 76),
('83', 'Var', 93),
('84', 'Vaucluse', 93),
('85', 'Vendée', 52),
('86', 'Vienne', 75),
('87', 'Haute-Vienne', 75),
('88', 'Vosges', 44),
('89', 'Yonne', 27),
('90', 'Territoire de Belfort', 27),
('91', 'Essonne', 11),
('92', 'Hauts-de-Seine', 11),
('93', 'Seine-Saint-Denis', 11),
('94', 'Val-de-Marne', 11),
('95', 'Val-d\'Oise', 11),
('971', 'Guadeloupe', 1),
('972', 'Martinique', 2),
('973', 'Guyane', 3),
('974', 'La Réunion', 4),
('976', 'Mayotte', 6);

-- --------------------------------------------------------

--
-- Structure de la table `historiquescore`
--

DROP TABLE IF EXISTS `historiquescore`;
CREATE TABLE IF NOT EXISTS `historiquescore` (
  `CollectiviteId` char(36) NOT NULL,
  `Score` int NOT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `CollectiviteId` (`CollectiviteId`),
  KEY `CollectiviteId_2` (`CollectiviteId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `opsn`
--

DROP TABLE IF EXISTS `opsn`;
CREATE TABLE IF NOT EXISTS `opsn` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(500) NOT NULL,
  `DepartementCode` char(3) NOT NULL,
  `Actif` int NOT NULL,
  `Logo` varchar(500) DEFAULT NULL,
  `Telephone` int DEFAULT NULL,
  `Mail` varchar(500) DEFAULT NULL,
  `Adresse` varchar(500) DEFAULT NULL,
  `Site_Internet` varchar(500) DEFAULT NULL,
  `Siret` char(14) DEFAULT NULL,
  `Latitude` varchar(500) DEFAULT NULL,
  `Longitude` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `opsn`
--

INSERT INTO `opsn` (`Id`, `Nom`, `DepartementCode`, `Actif`, `Logo`, `Telephone`, `Mail`, `Adresse`, `Site_Internet`, `Siret`, `Latitude`, `Longitude`) VALUES
('404', 'demo', '60', 1, NULL, NULL, 'demo@demo.fr', NULL, NULL, '44444444444444', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `opsn_departement`
--

DROP TABLE IF EXISTS `opsn_departement`;
CREATE TABLE IF NOT EXISTS `opsn_departement` (
  `OPSNId` char(36) NOT NULL,
  `DepartementCode` char(3) NOT NULL,
  PRIMARY KEY (`OPSNId`,`DepartementCode`),
  KEY `OPSNId` (`OPSNId`),
  KEY `DepartementCode` (`DepartementCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `opsn_departement`
--

INSERT INTO `opsn_departement` (`OPSNId`, `DepartementCode`) VALUES
('404', '60');

-- --------------------------------------------------------

--
-- Structure de la table `preference`
--

DROP TABLE IF EXISTS `preference`;
CREATE TABLE IF NOT EXISTS `preference` (
  `UtilisateurId` char(36) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Json` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `Id` char(36) NOT NULL,
  `IdTheme` char(36) NOT NULL,
  `Question` varchar(1000) DEFAULT NULL,
  `Ordre` int NOT NULL,
  `Definition` longtext,
  `IdCategorie` char(36) NOT NULL,
  `Multiple` tinyint(1) NOT NULL DEFAULT '0',
  `InfoComplementaire` longtext,
  `Titre_Definition` longtext,
  `IdParent` char(36) DEFAULT NULL,
  `IdRepParent` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `CategorieId` (`IdTheme`),
  KEY `IdCategorie` (`IdCategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`Id`, `IdTheme`, `Question`, `Ordre`, `Definition`, `IdCategorie`, `Multiple`, `InfoComplementaire`, `Titre_Definition`, `IdParent`, `IdRepParent`) VALUES
('ab3aa246-cd45-11ec-bd0e-0cc47a0ad120', '9c975984-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la télétransmission des actes au contrôle de légalité ?', 1, '', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 0, NULL, NULL, NULL, NULL),
('ab3bcfb5-cd45-11ec-bd0e-0cc47a0ad120', '9c97fdd6-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un profil acheteur ?', 1, '', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 0, NULL, NULL, NULL, NULL),
('ab3c6d7e-cd45-11ec-bd0e-0cc47a0ad120', '9c98cea5-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une gazette locale ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab3d0cf2-cd45-11ec-bd0e-0cc47a0ad120', '9c98cea5-cd39-11ec-bd0e-0cc47a0ad120', 'Votre gazette locale est-elle diffusée en ligne (sur votre site web, vos réseaux sociaux...) ?', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab3c6d7e-cd45-11ec-bd0e-0cc47a0ad120', 'ab3ce36b-cd45-11ec-bd0e-0cc47a0ad120'),
('ab3daf28-cd45-11ec-bd0e-0cc47a0ad120', '9c98cea5-cd39-11ec-bd0e-0cc47a0ad120', 'Votre gazette locale respecte-t-elle le RGAA ?', 3, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab3d0cf2-cd45-11ec-bd0e-0cc47a0ad120', 'ab3d84a8-cd45-11ec-bd0e-0cc47a0ad120'),
('ab3e4ea7-cd45-11ec-bd0e-0cc47a0ad120', '9c991f1e-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des boites de messagerie professionnelle pour les agents ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab3f04b8-cd45-11ec-bd0e-0cc47a0ad120', '9c991f1e-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des boites de messagerie professionnelle pour les élus ?', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab3fc5d7-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous dématérialisé les convocations de vos différentes réunions ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab408974-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de communication des documents des conseils communautaires avec les élus ?', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab411b09-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Votre structure a-t-elle mis en place des solutions de réunion à distance ?', 3, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab41dce1-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de travail collaboratif ?', 4, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab429d50-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'une solution d\'intranet ?', 5, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab435bd2-cd45-11ec-bd0e-0cc47a0ad120', '9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un réseau social interne (RSI) ?', 6, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab44208b-cd45-11ec-bd0e-0cc47a0ad120', '9c9ab4a4-cd39-11ec-bd0e-0cc47a0ad120', 'Votre collectivité a-t-elle une identité visuelle (logo et charte graphique) ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab44de34-cd45-11ec-bd0e-0cc47a0ad120', '9c9ab4a4-cd39-11ec-bd0e-0cc47a0ad120', 'Votre identité visuelle (logo et charte graphique) est-elle adaptable aux outils numériques actuels ?', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab44208b-cd45-11ec-bd0e-0cc47a0ad120', 'ab44af12-cd45-11ec-bd0e-0cc47a0ad120'),
('ab45a29b-cd45-11ec-bd0e-0cc47a0ad120', '9c9ab4a4-cd39-11ec-bd0e-0cc47a0ad120', 'Votre contenu visuel numérique (images, photographies, vidéos aériennes ou terrestres...) est-il de qualité pour votre site web ?', 3, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab466b2c-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un site internet ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab472fb0-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Envisagez-vous la mise en place d\'un nouveau service de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) ou souhaitez-vous procéder à la refonte d\'un existant ? ', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab47f73f-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous réalisé l\'audit de conformité au Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA) pour l\'ensemble de vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) ? ', 4, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab48f405-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous, pour l\'ensemble de vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques), indiqué leur état de conformité via leur page d\'accueil et publié les éléments suivants sur une page dédiée à l\'accessibilité : déclaration d\'accessibilité, schéma pluriannuel de mise en accessibilité et le plan d\'actions pour l\'année en cours, ainsi qu\'un moyen de contact permettant aux usagers d\'émettre des réclamations à ce sujet ?', 3, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab49bc5b-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Les documents bureautiques publiés via vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) sont-ils accessibles pour les personnes en situation de handicap ?', 5, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4a81dc-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un nom de domaine ?', 6, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4b3f51-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de prise de rendez-vous en ligne ?', 7, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4c039e-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'La collectivité est-elle présente sur un ou plusieurs réseaux sociaux ?', 8, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4cc48f-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Vos réseaux sociaux sont-ils affichés sur votre site web ?', 9, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab4c039e-cd45-11ec-bd0e-0cc47a0ad120', 'ab4c94fb-cd45-11ec-bd0e-0cc47a0ad120'),
('ab4d831e-cd45-11ec-bd0e-0cc47a0ad120', '9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', 'Votre site web dispose-t-il d\'un agent conversationnel (ou chatbot) ?', 10, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4e6414-cd45-11ec-bd0e-0cc47a0ad120', '9c9d882e-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une application d\'alerte SMS pour vos citoyens ?', 1, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4f252a-cd45-11ec-bd0e-0cc47a0ad120', '9c9d882e-cd39-11ec-bd0e-0cc47a0ad120', 'Utilisez-vous une application mobile d\'information aux citoyens ?', 2, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab4fe1b6-cd45-11ec-bd0e-0cc47a0ad120', '9c9d882e-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un système d\'inscription en ligne pour vos différents évènements communaux (repas, sorties ou animations diverses) ?', 3, '', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab508a12-cd45-11ec-bd0e-0cc47a0ad120', '9c9e25b2-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type École (musique, danse, ...) ?', 1, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab50e978-cd45-11ec-bd0e-0cc47a0ad120', '9c9e25b2-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 2, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab51a728-cd45-11ec-bd0e-0cc47a0ad120', '9c9e25b2-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 3, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab52653d-cd45-11ec-bd0e-0cc47a0ad120', '9c9e899a-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type Bibliothèque / Médiathèque ?', 1, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab52c214-cd45-11ec-bd0e-0cc47a0ad120', '9c9e899a-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion ?', 2, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab537b68-cd45-11ec-bd0e-0cc47a0ad120', '9c9f2669-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type Musée/Piscine ?', 1, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab53fe3b-cd45-11ec-bd0e-0cc47a0ad120', '9c9f2669-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion ?', 2, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab54c31b-cd45-11ec-bd0e-0cc47a0ad120', '9c9f2669-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une gestion de planning ?', 3, '', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5587b9-cd45-11ec-bd0e-0cc47a0ad120', '9c9fc5f0-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un suivi de la maintenance informatique ?', 1, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5649c0-cd45-11ec-bd0e-0cc47a0ad120', '9c9fc5f0-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un plan de renouvellement de votre parc informatique ?', 2, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5739f5-cd45-11ec-bd0e-0cc47a0ad120', '9c9fc5f0-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous fait un état des lieux de vos équipements informatiques ?', 3, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab57fd8e-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Êtes-vous homologué au Référentiel Général de Sécurité (RGS) ?', 1, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab58c456-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de sécurité informatique : Antivirus ou EDR (Endpoint Detection and Response) ?', 2, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5983fb-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Tous les ordinateurs possèdent-ils au minimum un anti-virus ?', 3, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab58c456-cd45-11ec-bd0e-0cc47a0ad120', 'ab5954a2-cd45-11ec-bd0e-0cc47a0ad120'),
('ab5a4305-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Tous les téléphones portables possèdent-ils au minimum un anti-virus ?', 4, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab58c456-cd45-11ec-bd0e-0cc47a0ad120', 'ab5954a2-cd45-11ec-bd0e-0cc47a0ad120'),
('ab5b0e90-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place sur tous les ordinateurs et serveurs un pare-feu ?', 5, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5bce94-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un pare-feu sur le réseau interne pour séparer votre système d\'information et internet ?', 6, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5c8c89-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de sécurité informatique (antispam) ?', 7, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5d4871-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de sécurité informatique (Proxy) ?', 8, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5e046c-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Sensibilisez-vous régulièrement vos agents aux risques liés à la cybersécurité ?', 9, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des systèmes de sauvegarde ?', 10, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab5f7e22-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une sauvegarde interne ?', 11, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', 'ab5f4f03-cd45-11ec-bd0e-0cc47a0ad120'),
('ab603d11-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de sauvegarde externalisée ?', 12, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', 'ab5f4f03-cd45-11ec-bd0e-0cc47a0ad120'),
('ab6102c3-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Faites-vous des tests de restauration de votre sauvegarde ?', 13, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', 'ab5f4f03-cd45-11ec-bd0e-0cc47a0ad120'),
('ab622f61-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une charte informatique ?', 14, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab62f221-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un outil de gestion de mots de passe sécurisé ?', 15, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab63ba83-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un inventaire de vos services numériques ?', 16, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab64778c-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'une politique de sécurité des systèmes d\'information ?', 17, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6539c5-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un plan de continuité d\'activité (PCA) ?', 18, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab65f8b2-cd45-11ec-bd0e-0cc47a0ad120', '9ca407db-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un plan reprise d\'activité (PRA) ?', 19, '', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab66e93a-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous équipé vos écoles de classe mobile ?', 1, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab67adf8-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous équipé vos écoles de solution robotique ?', 2, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab68933e-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils pédagogiques numériques pour les écoles maternelles ?', 3, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6959f9-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous équipé vos écoles primaires d\'un vidéoprojecteur, d\'un PC dédié et d\'un visualiseur ?', 4, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6a40fe-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des solutions interactives : TBI, VPI, ou ENI ? (Tableau Blanc Interactif, Vidéoprojecteur Interactif, Ecran Numérique Interactif)', 5, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6b00c8-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous équipé la direction de l\'école d\'un ordinateur, scanner, imprimante ?', 6, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6bc5ce-cd45-11ec-bd0e-0cc47a0ad120', '9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous établi un réseau informatique dans les écoles ?', 7, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6c8d84-cd45-11ec-bd0e-0cc47a0ad120', '9ca6e3e3-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type Périscolaire et restauration scolaire ?', 1, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6d497a-cd45-11ec-bd0e-0cc47a0ad120', '9ca6e3e3-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 2, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6e0f63-cd45-11ec-bd0e-0cc47a0ad120', '9ca6e3e3-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 3, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6ecea6-cd45-11ec-bd0e-0cc47a0ad120', '9ca6e3e3-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de pointage (tablette, code-barres, borne) ?', 4, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab6f8e67-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type Petite enfance / crèche ?', 1, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab71106b-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 1, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab71d449-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 2, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab729703-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de pointage (tablette, code-barres, borne) ?', 3, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab73559a-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type RAM (relais d\'assistantes maternelles) ?', 4, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab741aa6-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 5, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab752112-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 6, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab75dde3-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous une structure de type RAM itinérante ?', 7, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab76bdd0-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion ?', 8, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab777f6d-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un logiciel de gestion de parc automobile?', 9, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab784323-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une gestion de planning ?', 10, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7902bb-cd45-11ec-bd0e-0cc47a0ad120', '9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un portail web ?', 11, '', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab79c351-cd45-11ec-bd0e-0cc47a0ad120', '9caa40b5-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous un établissement de type Spanc / eau / assainissement ?', 1, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7a8441-cd45-11ec-bd0e-0cc47a0ad120', '9caa40b5-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion et de facturation ?', 2, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7b65f2-cd45-11ec-bd0e-0cc47a0ad120', '9caa40b5-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 3, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7c2992-cd45-11ec-bd0e-0cc47a0ad120', '9caa40b5-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion des contrôles des assainissements non collectifs (ANC) ?', 4, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7cec01-cd45-11ec-bd0e-0cc47a0ad120', '9cab38e8-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous un les déchets dans votre structure ?', 1, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7dacda-cd45-11ec-bd0e-0cc47a0ad120', '9cab38e8-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion et de facturation ?', 2, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7e6db0-cd45-11ec-bd0e-0cc47a0ad120', '9cab38e8-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion de bac ?', 3, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7f2b73-cd45-11ec-bd0e-0cc47a0ad120', '9cab38e8-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 4, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab7feb54-cd45-11ec-bd0e-0cc47a0ad120', '9cabe3b2-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un Système d\'Information Géographique (SIG) ?', 1, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab81de29-cd45-11ec-bd0e-0cc47a0ad120', '9cabe3b2-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution SIG gérée en interne ?', 2, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab82c6d8-cd45-11ec-bd0e-0cc47a0ad120', '9cabe3b2-cd39-11ec-bd0e-0cc47a0ad120', 'Votre solution SIG est-elle accessible depuis le web via un navigateur ?', 3, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab838904-cd45-11ec-bd0e-0cc47a0ad120', '9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous un établissement de gestion de transport ?', 1, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab84304d-cd45-11ec-bd0e-0cc47a0ad120', '9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de gestion et de facturation dédiée au transport ?', 2, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab84dab1-cd45-11ec-bd0e-0cc47a0ad120', '9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 3, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab858318-cd45-11ec-bd0e-0cc47a0ad120', '9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de gestion de parc ?', 4, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab864a5b-cd45-11ec-bd0e-0cc47a0ad120', '9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de gestion de planning de chauffeur ?', 5, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab86f914-cd45-11ec-bd0e-0cc47a0ad120', '9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de gestion des dossiers d\'urbanisme (DP, DT, PC ...) ?', 1, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab87bcca-cd45-11ec-bd0e-0cc47a0ad120', '9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'Votre Plan Local d\'Urbanisme (PLU) est-il disponible en ligne pour vos citoyens ?', 2, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab888094-cd45-11ec-bd0e-0cc47a0ad120', '9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un portail web permettant au pétitionnaire de déposer une autorisation d\'urbanisme ?', 3, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab894246-cd45-11ec-bd0e-0cc47a0ad120', '9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la possibilité de signer les décisions d\'urbanisme via un parapheur électronique ?', 4, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8a455b-cd45-11ec-bd0e-0cc47a0ad120', '9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place une solution de Saisine par Voie Electronique ?', 5, '', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8aef0e-cd45-11ec-bd0e-0cc47a0ad120', '9caec6db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous une solution informatique de gestion des contacts ?', 1, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8be8ed-cd45-11ec-bd0e-0cc47a0ad120', '9caec6db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous désigné un délégué à la protection des données (DPO) ?', 2, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8cd19f-cd45-11ec-bd0e-0cc47a0ad120', '9caec6db-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous constitué votre registre des activités de traitement ?', 3, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8dae6d-cd45-11ec-bd0e-0cc47a0ad120', '9caf29a2-cd39-11ec-bd0e-0cc47a0ad120', 'Numérisez-vous les courriers entrants dans votre structure ?', 1, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8e737b-cd45-11ec-bd0e-0cc47a0ad120', '9caf29a2-cd39-11ec-bd0e-0cc47a0ad120', 'Disposez-vous d\'un outil de gestion des documents ?', 2, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8f31a4-cd45-11ec-bd0e-0cc47a0ad120', '9cb00270-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous des outils pour le transfert de fichiers volumineux ?', 1, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab8fee05-cd45-11ec-bd0e-0cc47a0ad120', '9cb00270-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous fait un état des lieux de votre débit internet ?', 2, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab90a9fd-cd45-11ec-bd0e-0cc47a0ad120', '9cb00270-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des bornes wifi à disposition du public dans vos bâtiments communaux ?', 3, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab916714-cd45-11ec-bd0e-0cc47a0ad120', '9cb00270-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des bornes wifi public dans vos espaces communaux ouverts ?', 4, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab922471-cd45-11ec-bd0e-0cc47a0ad120', '9cb0d711-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils informatiques en libre accès de type médiathèque ?', 1, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab92e729-cd45-11ec-bd0e-0cc47a0ad120', '9cb0d711-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils informatiques en libre accès du type poste public à l\'accueil ?', 2, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab93a628-cd45-11ec-bd0e-0cc47a0ad120', '9cb0d711-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils informatiques d\'affichage numérique ?', 3, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9461e7-cd45-11ec-bd0e-0cc47a0ad120', '9cb0d711-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils informatiques en libre accès du type borne d\'information ?', 4, '', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab951f7d-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion de comptabilité ?', 1, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab95dc09-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la dématérialisation des échanges avec votre comptable ?', 2, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab969c30-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la signature électronique des flux comptables à destination du trésor public ?', 3, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9758b7-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un parapheur électronique ?', 4, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9839ae-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la validation des pièces justificatives via un parapheur électronique ?', 5, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab992690-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un connecteur Chorus ?', 6, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9a3789-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un connecteur GED en lien avec votre comptabilité ?', 7, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9b0e05-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place le PES RETOUR entre votre logiciel de comptabilité et votre trésorerie ?', 8, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9bdb9d-cd45-11ec-bd0e-0cc47a0ad120', '9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la dématérialisation des budgets auprès du contrôle de légalité et de votre trésorerie ?', 9, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9ca347-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion de l\'état civil ?', 1, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9d331f-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la déclaration des décès en ligne via l\'application CertDC', 2, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9dbc4b-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', ' Avez-vous mis en place une solution de Saisine par Voie Electronique ? ', 3, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9e75f6-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la démarche simplifiée pour vos démarches administratives ?', 4, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9f4866-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous fait numériser vos actes d\'état civil ?', 5, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('ab9fc45c-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Les actes numérisés sont-ils intégrés à votre solution logiciel d\'état civil ?', 6, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba03e0a-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion des élections ?', 7, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba0ba4b-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion du recensement citoyen ?', 8, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba13464-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion du cimetière ?', 9, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba1b056-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un plan de cimetière informatisé en lien avec votre logiciel de gestion ?', 10, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba22a3d-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la dématérialisation des échanges Insee en lien avec l\'état civil ?', 11, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba2a49a-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la dématérialisation des échanges PECOTO en lien avec le recensement citoyen ?', 12, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba31d91-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Utilisez- vous votre logiciel de gestion élections pour effectuer le tirage au sort des jurés d\'assises ?', 13, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba395c1-cd45-11ec-bd0e-0cc47a0ad120', '9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place COMEDEC (COMmunication Electronique des Données de l\'État Civil) ? ', 14, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba4108f-cd45-11ec-bd0e-0cc47a0ad120', '9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous un logiciel de gestion des ressources humaines ?', 1, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba4d8fc-cd45-11ec-bd0e-0cc47a0ad120', '9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un portail agent (gestion des congés) ?', 2, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba596d8-cd45-11ec-bd0e-0cc47a0ad120', '9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la DSN ?', 3, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba65523-cd45-11ec-bd0e-0cc47a0ad120', '9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place la dématérialisation des bulletins de paie ?', 4, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba716e2-cd45-11ec-bd0e-0cc47a0ad120', '9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un connecteur DSN pour récuperer l\'ensemble de vos déclarations automatiquement ?', 5, '', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba7d939-cd45-11ec-bd0e-0cc47a0ad120', '9cb85204-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous des logement sociaux ?', 1, '', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba89f60-cd45-11ec-bd0e-0cc47a0ad120', '9cb85204-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion ?', 2, '', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('aba95415-cd45-11ec-bd0e-0cc47a0ad120', '9cb90a5b-cd39-11ec-bd0e-0cc47a0ad120', 'Gérez-vous un établissement de type RPA', 1, '', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('abaa002a-cd45-11ec-bd0e-0cc47a0ad120', '9cb90a5b-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place des outils de gestion et de facturation ?', 2, '', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL),
('abaa9406-cd45-11ec-bd0e-0cc47a0ad120', '9cb90a5b-cd39-11ec-bd0e-0cc47a0ad120', 'Avez-vous mis en place un système de paiement en ligne ?', 3, '', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `recommandation`
--

DROP TABLE IF EXISTS `recommandation`;
CREATE TABLE IF NOT EXISTS `recommandation` (
  `Id` char(36) NOT NULL,
  `Titre` varchar(5000) DEFAULT NULL,
  `Text` longtext,
  `IdQuestion` char(36) NOT NULL,
  `IdCategorie` char(36) NOT NULL,
  `IdReponse` char(36) NOT NULL,
  `NiveauReco` int NOT NULL DEFAULT '1',
  `StatutReco` int DEFAULT '4',
  PRIMARY KEY (`Id`),
  KEY `IdQuestion` (`IdQuestion`),
  KEY `IdCategorie` (`IdCategorie`),
  KEY `IdReponse` (`IdReponse`),
  KEY `StatutReco` (`StatutReco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `recommandation`
--

INSERT INTO `recommandation` (`Id`, `Titre`, `Text`, `IdQuestion`, `IdCategorie`, `IdReponse`, `NiveauReco`, `StatutReco`) VALUES
('34b05f67-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la télétransmission des actes au contrôle de légalité ?', 'Il s\'agit de la mise en place de la transmission dématérialisée des délibérations et des arrêtés de votre collectivité auprès du contrôle de légalité. Elle permet un gain de temps car la validation de la préfécture et quasi immédiate.', 'ab3aa246-cd45-11ec-bd0e-0cc47a0ad120', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 'ab3bbb43-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1610b-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un profil acheteur ?', 'Un profil acheteur est obligatoire pour les collectivités déposant des dossiers de consultation au vu de la passation d\'un marché public. Ce profil acheteur doit permettre le dépôt et la réception des documents liés à la passation des marchés.', 'ab3bcfb5-cd45-11ec-bd0e-0cc47a0ad120', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 'ab3c58f9-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b17279-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une gazette locale ?', 'Utilisées depuis plusieurs décennies, les gazettes locales (anciennement appelées des bulletins municipaux) sont toujours appréciées des administrés. Elles permettent d\'informer les administrés, de toucher un maximum de personnes et elles sont l\'un des outils de communication le plus efficace. Si vous n\'avez pas de gazette locale, c\'est le moment de l\'envisager. Si vous en avez une physique que vous distribuez, n\'oubliez pas qu\'il est facile de la mettre en ligne !', 'ab3c6d7e-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab3cf810-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b17ead-cd79-11ed-b976-0cc47a39c2c0', 'Votre gazette locale est-elle diffusée en ligne (sur votre site web, vos réseaux sociaux...) ?', 'Certes les éditions papiers sont attendues dans les boîtes aux lettres, mais la distribution et la mise en ligne ne sont pas incompatibles. Puisque votre gazette locale sera certainement créée informatiquement, il est tout à fait possible de l\'éditer et de la publier en ligne.', 'ab3d0cf2-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab3d995c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b18a2e-cd79-11ed-b976-0cc47a39c2c0', 'Votre gazette locale respecte-t-elle le RGAA ?', 'En tant qu\'organisme public, le décret n°2019-768 renforce les obligations en matière d\'accessibilité de vos services de communication (site internet, intranet, application mobile...). Si votre gazette locale est publiée sur l\'un de vos services en ligne, elle est soumise aux règles du Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA).', 'ab3daf28-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab3e3b22-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b194ee-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des boites de messagerie professionnelle pour les agents ?', 'La création de boite mail professionnelle pour vos agents permet d\'avoir un plus grand espace de stockage, de sécuriser les échanges et d\'optimiser la confiance des administrés en la liant au domaine de votre collectivité. Cela permet également de séparer les activités professionnelles et personnelles.', 'ab3e4ea7-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab3eeb0f-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1a04e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des boites de messagerie professionnelle pour les élus ?', 'La création de boite mail professionnelle pour vos élus permet d\'avoir un plus grand espace de stockage, de sécuriser les échanges et d\'optimiser la confiance des administrés en la liant au domaine de votre collectivité. Cela permet également de séparer les activités professionnelles et personnelles.', 'ab3f04b8-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab3fabba-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1aa2a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous dématérialisé les convocations de vos différentes réunions ?', 'Depuis la modification de la loi Engagement et Proximité, l\'article 9 modifie la norme. Désormais les collectivités doivent envoyer les convocations par voie dématérialisée. Des solutions existent pour dématérialiser ces envois mais également pour y ajouter les documents de réunion.', 'ab3fc5d7-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab406e5f-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1b50e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de communication des documents des conseils communautaires avec les élus ?', 'S\'ils ne siègent pas au conseil communautaire, les élus municipaux ne connaissent pas toujours les décisions qui sont prises au sein de leur EPCI. Pour favoriser une meilleure circulation de l\'information, les convocations, rapports et comptes rendus des réunions de l\'intercommunalité seront adressés par voie électronique à tous les élus des conseils municipaux des communes (article 8 de la loi Engagement en Proximité). ', 'ab408974-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab41017c-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b1c034-cd79-11ed-b976-0cc47a39c2c0', 'Votre structure a-t-elle mis en place des solutions de réunion à distance ?', 'Afin d\'optimiser la participation à vos réunions et de faciliter l\'accès et la disponibilité des participants, vous pouvez mettre en place des solutions de visioconférence. Des solutions logiciel et matériel vous permettent d\'organiser des réunions à distance.', 'ab411b09-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab41c155-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1c95f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de travail collaboratif ?', 'Plusieurs études montrent que la mise en place d\'outils collaboratifs a plusieurs vertus. De l\'amélioration de la communication à l\'accessibilité des données, ce type d\'outils est tout à fait adapté pour rendre votre système d\'information optimal.', 'ab41dce1-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4283d3-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1d2c7-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'une solution d\'intranet ?', 'Un intranet est un site interne à votre structure, accessible uniquement par vos agents. Il contient vos informations. Vous pouvez accéder à vos fichiers, organiser votre travail et collaborer avec des collègues, ainsi qu\'être au courant des actualités de votre structure.', 'ab429d50-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab433f58-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b1dce0-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un réseau social interne (RSI) ?', 'Un Réseau Social est  une communauté de personnes appartenant à une même entité et échangeant à des fins professionnelles. Cela a pour objectif de rendre le travail plus collaboratif et la communication intra plus fluide. Seuls les agents et les élus ont accès à ce dispositif. Le but est de faciliter les échanges, notamment entre différents services, différents domaines d\'expertise, sans notion particulière de hiérarchie.', 'ab435bd2-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4404cf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b1e77f-cd79-11ed-b976-0cc47a39c2c0', 'Votre collectivité a-t-elle une identité visuelle (logo et charte graphique) ?', 'Vos services de communication en ligne (site web, réseaux sociaux, application mobile...) sont des outils importants pour la vie locale. S\'ils ne sont pas chartés et cohérents visuellement, les administrés se demanderont si l\'information est réellement officielle et fiable. Afin de pallier cet effet négatif, une identité visuelle incluant un logo percutant est indispensable.', 'ab44208b-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab44c49e-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1f158-cd79-11ed-b976-0cc47a39c2c0', 'Votre identité visuelle (logo et charte graphique) est-elle adaptable aux outils numériques actuels ?', 'Certaines collectivités sont représentées par des blasons historiquement mis en place depuis plusieurs centaines d\'années. Parfois, cette identité visuelle est non exploitable numériquement (pixelisée, en noir et blanc, de mauvaise qualité...). Il est alors nécessaire de la retravailler ou de la refondre totalement pour plus de modernité !', 'ab44de34-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab45855c-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b1fbe8-cd79-11ed-b976-0cc47a39c2c0', 'Votre contenu visuel numérique (images, photographies, vidéos aériennes ou terrestres...) est-il de qualité pour votre site web ?', '\"94% des articles web les plus lus sont composés de visuels\" (Étude lecadredigital.fr de 2019). En plus de l\'aspect esthétique, il est alors important pour vous de produire des visuels de qualité et de les intégrer sur votre site web. En bonus, ces visuels pourront être utilisés sur vos éventuels réseaux sociaux et ils mettront en avant la collectivité et tous ses atouts.', 'ab45a29b-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab464e9a-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2073f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un site internet ?', 'Aujourd\'hui environ 85 % des administrés naviguent sur internet depuis leur ordinateur ou leur téléphone portable. La création d\'un site internet permet de partager des informations relatives à la vie de la commune au plus grand nombre de personnes. La mise en ligne d\'un site doit également répondre à plusieurs règles comme l\'accessibilité, ou la publication de documents obligatoires (urbanisme, compte rendu de conseil, etc..).', 'ab466b2c-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab471651-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b21390-cd79-11ed-b976-0cc47a39c2c0', 'Envisagez-vous la mise en place d\'un nouveau service de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) ou souhaitez-vous procéder à la refonte d\'un existant ? ', 'Votre organisme a des obligations à respecter en vertu de la loi n°2005-102 et du décret n°2019-768 afin que ses services de communication au public en ligne soient accessibles aux personnes handicapées. Cela implique de respecter un ensemble de règles posées par le Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA). Nombre d\'entre elles concernent la structure même de votre service (son code), aussi est-il impératif d\'en tenir compte dès sa conception et tout au long de son développement, au risque de ne pas pouvoir corriger les malfaçons par la suite. \r\nC\'est pourquoi il est essentiel de choisir un prestataire au fait de ces obligations et en capacité de les intégrer au service développé. Dans le cadre d\'un marché public, cela doit se traduire dans le cahier des charges afin d\'obtenir des garanties et de choisir le candidat adéquat. ', 'ab472fb0-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab47dcba-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b21fa6-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous réalisé l\'audit de conformité au Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA) pour l\'ensemble de vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) ? ', 'Il est nécessaire, conformément à la loi n°2005-102 et au décret n°2019-768, de réaliser un audit de vos services de communication au public en ligne et de les mettre en conformité avec les règles posées par le RGAA. \r\nLes services publics numériques et certains services privés ont en effet l\'obligation d\'être accessibles de façon équivalente à tout citoyen, qu\'il soit ou non en situation de handicap (visuel, auditif, moteur, trouble dys...). Un service numérique accessible est plus facile à utiliser pour les personnes handicapées et de meilleure qualité pour tous.', 'ab47f73f-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab48da1e-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b22b7a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous, pour l\'ensemble de vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques), indiqué leur état de conformité via leur page d\'accueil et publié les éléments suivants sur une page dédiée à l\'accessibilité : déclaration d\'accessibilité, schéma pluriannuel de mise en accessibilité et le plan d\'actions pour l\'année en cours, ainsi qu\'un moyen de contact permettant aux usagers d\'é', 'En complément de l\'audit de conformité au Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA) de vos services de communication au public en ligne, vous êtes tenus de publier par leur biais l\'ensemble de ces éléments afin de remplir vos obligations telles que définies par la loi n°2005-102 et le décret n°2019-768. \r\nAu-delà de cet aspect déclaratif, votre organisme doit s\'engager plus globalement dans une démarche d\'amélioration continue destinée à favoriser toujours plus d\'accessibilité numérique. ', 'ab48f405-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab49a2d3-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b23772-cd79-11ed-b976-0cc47a39c2c0', 'Les documents bureautiques publiés via vos services de communication au public en ligne (sites internet, extranets, intranets, applications mobiles, progiciels web ou mobiles et mobiliers urbains numériques) sont-ils accessibles pour les personnes en situation de handicap ?', 'En tant qu\'organisme public, la loi n°2005-102 et le décret n°2019-768 posent les obligations en matière d\'accessibilité de vos services de communication au public en ligne. Si vous publiez des documents bureautiques par leur biais (gazette locale, comptes-rendus des instances délibérantes, formulaires à télécharger, etc.), ils doivent être rédigés conformément aux règles du Référentiel Général d\'Amélioration de l\'Accessibilité (RGAA).', 'ab49bc5b-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4a67c9-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b24161-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un nom de domaine ?', 'Le nom de domaine permet de donner une meilleure visibilité au site Internet, non seulement parce qu\'il permet à l\'utilisateur de mémoriser plus facilement l\'adresse du site en question, mais parce qu\'un nom de domaine pertinent est indexé plus efficacement par les moteurs de recherches.', 'ab4a81dc-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4b261a-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b24a8f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de prise de rendez-vous en ligne ?', 'Aujourd\'hui 77% des Français ont déjà effectué une réservation en ligne cela montre à quel point le digital est présent dans notre quotidien. La prise de rendez-vous en ligne permet en quelques clics de prendre rendez-vous avec votre administration de façon autonome pour vos administrés et cela peu importe l\'heure ou le jour.', 'ab4b3f51-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4be333-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b25b9a-cd79-11ed-b976-0cc47a39c2c0', 'La collectivité est-elle présente sur un ou plusieurs réseaux sociaux ?', 'Même s\'il n\'est pas forcément pertinent pour vous de créer un profil social sur plusieurs réseaux sociaux différents, il est conseillé d\'en ouvrir au moins un pour améliorer la visibilité de vos informations locales. Le tout est de se créer un espace sur le bon réseau, de pouvoir le gérer correctement et surtout de savoir comment l\'alimenter. Chaque collectivité, ou même chaque structure publique (médiathèque, accueil de loisirs, école, etc.) a son besoin personnel en fonction de sa cible et de ce qu\'elle souhaite communiquer, quand, comment et où.', 'ab4c039e-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4caacf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2660e-cd79-11ed-b976-0cc47a39c2c0', 'Vos réseaux sociaux sont-ils affichés sur votre site web ?', 'Pour améliorer votre visibilité et faciliter l\'accès à vos différentes réseaux sociaux, il est désormais possible d\'ajouter des liens (en entête, en pied de page, en page d\'accueil...) directement sur votre site web.', 'ab4cc48f-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4d69d2-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b26fc7-cd79-11ed-b976-0cc47a39c2c0', 'Votre site web dispose-t-il d\'un agent conversationnel (ou chatbot) ?', 'Les chatbots offrent un service intégré, là où les services disponibles par la collectivité ne sont pas forcément accessibles, selon les horaires d\'ouverture au public par exemple. Les citoyens peuvent ainsi accéder directement, via un personnage en ligne, à l\'information en temps réel dont ils ont besoin. Côté collectivité, le bot est tout aussi positif, il favorise la satisfaction des citoyens, apporte un gain de temps considérable et améliore l\'image de la collectivité. ', 'ab4d831e-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4e4905-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b27a3f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une application d\'alerte SMS pour vos citoyens ?', 'Selon l\'article L.2212-2 du Code Général des Collectivités Territoriales, en vertu de ses pouvoirs de police, le maire a l\'obligation de prévenir la population en cas de danger imminent. La mise en place d\'une application mobile à destination des administrés permettant la diffusion d\'alerte par SMS répond à cela mais permet également de pousser de l\'information selon les critères choisis par les citoyens.', 'ab4e6414-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4f0ab0-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b28506-cd79-11ed-b976-0cc47a39c2c0', 'Utilisez-vous une application mobile d\'information aux citoyens ?', '\"En France 64,70 millions de personnes sont équipées de mobile ce qui représente 99% de la population\" (Étude comarketing-news de 2019). Vos administrés seront donc très certainement enclins à naviguer sur une application mobile diffusant de l\'information liée à l\'activité de votre collectivité.', 'ab4f252a-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab4fc84c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b28e9e-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un système d\'inscription en ligne pour vos différents évènements communaux (repas, sorties ou animations diverses) ?', 'Si quelques administrés aiment s\'inscrire à vos évènements en se déplaçant dans votre collectivité ou par voie postale, de plus en plus préfèreront pouvoir le faire partout et à n\'importe quel moment. Mettre en place un système d\'inscription en passant par un outil de Gestion des Relations avec les Citoyens (GRC) peut simplifier la vie des citoyens et des agents.', 'ab4fe1b6-cd45-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'ab50706e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b29822-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab50e978-cd45-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'ab518dee-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2a27b-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab51a728-cd45-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'ab524b61-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2ac79-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion ?', 'Un logiciel de gestion vous permet différentes choses comme la saisie des prêts, des retours et des réservations, d\'effectuer le catalogage, l\'exemplarisation, la gestion des périodiques et également de gérer des statistiques.', 'ab52c214-cd45-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'ab5361cf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2b5e6-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion ?', 'La mise en place d\'un logiciel de gestion vous permettra notamment de sécuriser vos données, de gérer votre billetterie ou vos réservations. Il pourra également être interfacé avec votre portail citoyen pour vos administrés.', 'ab53fe3b-cd45-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'ab54a9a6-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2bef3-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une gestion de planning ?', 'La mise en place d\'un logiciel de gestion de planning permet de vous faire gagner du temps et de la visibilité sur vos équipes. Il permet également d\'offrir beaucoup plus de flexibilité à vos agents pour la saisie de leurs demandes. ', 'ab54c31b-cd45-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'ab556e0e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b2ef75-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un suivi de la maintenance informatique ?', 'Le suivi de la maintenance informatique en interne ou externe permet de maintenir du matériel spécifique qui nécessite des compétences pointues. Un équipement non maintenu a plus de chance de tomber en panne et sa remise en état peut s\'avérer longue et coûteuse. ', 'ab5587b9-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab562fcd-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b2faab-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un plan de renouvellement de votre parc informatique ?', 'Aujourd\'hui la mise en place d\'un plan de renouvellement informatique est une nécessité. Cela permet d\'avoir du matériel informatique performant et qui répond à vos besoins mais également d\'avoir de la visibilité sur les investissements à prévoir pour vos budgets.', 'ab5649c0-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab571f8e-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b30618-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous fait un état des lieux de vos équipements informatiques ?', 'Un état des lieux ou audit informatique permet d\'avoir un avis extérieur sur le système d\'information à un instant T. Il met en évidence les failles de sécurité, permet d\'être alerté sur l\'obsolescence du matériel, permet d\'avoir des conseils ainsi qu\'un plan d\'action pour améliorer le parc informatique. C\'est un outil décisionnel important.', 'ab5739f5-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab57e35a-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b31111-cd79-11ed-b976-0cc47a39c2c0', 'Êtes-vous homologué au Référentiel Général de Sécurité (RGS) ?', 'Les autorités administratives, telles que les collectivités doivent garantir la sécurité de leurs systèmes d\'information pour favoriser la confiance des usagers dans les services électroniques qu\'elles mettent à leur disposition. Dès lors, le RGS (pris en application du décret n° 2010-112 du 2 février 2010) s\'impose aux autorités administratives qui utilisent un système d\'information dans leurs relations entre elles et dans leurs relations avec les usagers (exemple : les téléservices). De plus, une démarche d\'homologation au RGS permet de procéder à une analyse de risques qui permet d\'identifier les menaces et définir un plan d\'action pour les rendre acceptables.', 'ab57fd8e-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab58a9fd-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b31b46-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de sécurité informatique : Antivirus ou EDR (Endpoint Detection and Response) ?', 'Pour aider à vous protéger des codes malveillants et des attaques, il est recommandé d\'installer un anti-virus sur chaque objet composant le système d\'information.', 'ab58c456-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab596ab1-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3255f-cd79-11ed-b976-0cc47a39c2c0', 'Tous les ordinateurs possèdent-ils au minimum un anti-virus ?', 'Il est recommandé d\'avoir un anti-virus sur chaque ordinateur. Un ordinateur sans protection constitue une faille de sécurité pour le système d\'information.', 'ab5983fb-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5a29cb-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b32e73-cd79-11ed-b976-0cc47a39c2c0', 'Tous les téléphones portables possèdent-ils au minimum un anti-virus ?', 'Il est recommandé d\'avoir un anti-virus sur chaque téléphone portable. Un téléphone portable sans protection constitue une faille de sécurité pour le système d\'information. Pour aller plus loin, référez-vous aux chapitres 14 et 33 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5a4305-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5af4c2-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b33815-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place sur tous les ordinateurs et serveurs un pare-feu ?', 'La mise en place d\'un pare-feu sur chaque ordinateur et serveur permet d\'éviter la propagation d\'une attaque dans le système d\'information. Pour aller plus loin, référez-vous au chapitre 17 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5b0e90-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5bb49e-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3417d-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un pare-feu sur le réseau interne pour séparer votre système d\'information et internet ?', 'Il est recommandé la mise en place d\'un pare-feu afin d\'éviter une attaque directe sur votre système d\'information. Pour aller plus loin, référez-vous au chapitre 19 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5bce94-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5c7363-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b34b36-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de sécurité informatique (antispam) ?', 'Il est recommandé de mettre en place une solution antispam sur l\'ensemble du parc. Elle permet aux utilisateurs d\'éviter de cliquer sur un lien malveillant. Pour aller plus loin, référez-vous au chapitre 24 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5c8c89-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5d2ee3-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b35353-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de sécurité informatique (Proxy) ?', 'Il est recommandé de mettre en place un proxy. Il s\'agit d\'une passerelle qui permet de sécuriser les accès à internet. Pour aller plus loin, référez-vous au chapitre 22 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5d4871-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5deb38-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b35a8c-cd79-11ed-b976-0cc47a39c2c0', 'Sensibilisez-vous régulièrement vos agents aux risques liés à la cybersécurité ?', 'Il est recommandé de sensibiliser régulièrement tous les utilisateurs aux risques cyber. Cette action permet aux utilisateurs de comprendre les enjeux et d\'adopter les bons comportements. A noter que le risque d\'une erreur humaine n\'est pas négligeable. Pour aller plus loin, référez-vous au chapitre 2 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5e046c-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5ea84d-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3629e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des systèmes de sauvegarde ?', 'Il est recommandé la mise en place d\'un système de sauvegarde de vos données avec la règle 3-2-1-0. Il s\'agit de disposer de trois copies de vos données au moins, de stocker ces copies sur deux supports différents et de conserver une copie de la sauvegarde hors site.', 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab5f647e-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b36a27-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une sauvegarde interne ?', 'Il est recommandé de mettre en place un système de sauvegarde de vos données en interne pour une restauration rapide. Pour aller plus loin, référez-vous au chapitre 4 du guide d\'hygiène informatique de l\'ANSSI.', 'ab5f7e22-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab60235a-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b371dc-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de sauvegarde externalisée ?', 'Il est recommandé de mettre en place une sauvegarde de vos données à l\'extérieur de vos locaux sur un serveur hébergé et sécurisé pour éviter qu\'un attaquant puisse les modifier. Pour aller plus loin, référez-vous au chapitre 4 du guide d\'hygiène informatique de l\'ANSSI.', 'ab603d11-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab60e856-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b378a4-cd79-11ed-b976-0cc47a39c2c0', 'Faites-vous des tests de restauration de votre sauvegarde ?', 'Il est recommandé  de vérifier vos sauvegardes en les restaurant pour vérifier l\'intégrité de la donnée. Pour aller plus loin, référez-vous au chapitre 4 du guide d\'hygiène informatique de l\'ANSSI.', 'ab6102c3-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab621384-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3957e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une charte informatique ?', 'Une charte informatique est un outil favorisant la prévention des incidents et attaques informatiques par la mise en place de bonnes pratiques qui concourent à la protection globale du système d\'information. Par exemple, elle permet notamment de définir la politique de mots de passe que les utilisateurs doivent appliquer.', 'ab622f61-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab62d8b8-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3a9b5-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un outil de gestion de mots de passe sécurisé ?', 'Une liste de mots de passe papier ou numérique constitue une faille de sécurité importante dès lors qu\'elle peut être récupérée par une personne malveillante. \r\nPour pallier le besoin de répertorier les mots de passe tout en maintenant un niveau de sécurité adéquat, veillez à ne pas enregistrer par défaut les mots de passe sur les navigateurs et applications mais optez pour l\'utilisation d\'un gestionnaire de mots de passe.\r\nCet outil permet d\'adopter une politique de mot de passe rigoureuse notamment en facilitant :\r\n-	l\'utilisation d\'un mot de passe unique pour chaque service ;\r\n-	le renouvellement des mots de passe régulièrement ;\r\n-	la création de mots de passe complexes.\r\nPour aller loin, référez-vous à la délibération n° 2017-012 du 19 janvier 2017 portant adoption d\'une recommandation relative aux mots de passe.', 'ab62f221-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab639f8f-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3be1d-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un inventaire de vos services numériques ?', 'Il est recommandé d\'avoir un listing de vos services numériques utilisés en précisant quels sont les utilisateurs, les administrateurs et les prestataires ayant accès. Aussi, il est nécessaire de la mettre à jour fréquemment. Pour aller plus loin, référez-vous au chapitre 5 du guide d\'hygiène informatique de l\'ANSSI.', 'ab63ba83-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab645e59-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b3cff4-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'une politique de sécurité des systèmes d\'information ?', 'La politique de sécurité des systèmes d\'information (PSSI) est un plan d\'actions défini pour maintenir un certain niveau de sécurité. Elle reflète la vision stratégique de la direction de votre collectivité en matière de sécurité des systèmes d\'information (SSI).', 'ab64778c-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab651e0c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b3e560-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un plan de continuité d\'activité (PCA) ?', 'Un plan de continuité d\'activité (PCA) a pour but de garantir la continuité des services en cas de sinistre important touchant votre système d\'information.', 'ab6539c5-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab65dd72-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b3fc8b-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un plan reprise d\'activité (PRA) ?', 'Un plan de reprise d\'activité (PRA) a pour but, après un sinistre important touchant votre système d\'information, de reprendre l\'activité le plus rapidement possible avec le minimum de perte de données.', 'ab65f8b2-cd45-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'ab66cd73-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b41031-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous équipé vos écoles de classe mobile ?', 'Une classe mobile est généralement constituée d\'un caisson à roulettes dans laquelle les équipements informatiques sont stockés et rechargés. Elle permet de réduire les coûts engendrés par l\'équipement d\'une salle informatique dédiée. Les enseignants ne sont plus restreints dans l\'élaboration de leurs cours et disposent d\'outils de supervision pour suivre l\'évolution de leurs élèves.', 'ab66e93a-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab67942d-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b424c8-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous équipé vos écoles de solution robotique ?', 'Les solutions robotiques sont des supports innovants pour l\'apprentissage, une nouvelle forme de pédagogie qui commence à se démocratiser. Selon une étude, 92% des enseignants remarquent une nette hausse de la motivation chez les élèves, lorsqu\'un robot est utilisé en cours.', 'ab67adf8-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab68565d-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b43edb-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils pédagogiques numériques pour les écoles maternelles ?', 'Il est possible d\'équiper les écoles maternelles d\'équipements numériques en lien avec le socle numérique recommandé par l\'Education nationale. Il est bien sûr important de respecter l\'âge des enfants en respectant la loi \"Abeille\" n°2015-136 par exemple.', 'ab68933e-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab693f8b-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b4572a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous équipé vos écoles primaires d\'un vidéoprojecteur, d\'un PC dédié et d\'un visualiseur ?', 'Le socle numérique préconise que chaque salle de classe soit équipée de base d\'un vidéoprojecteur, d\'un poste de travail et en option d\'un visualiseur. Le vidéoprojecteur est aujourd\'hui indispensable pour les activités communes, l\'ordinateur va servir à piloter les périphériques, accéder à internet et exploiter les ressources et les services en ligne. Pour le visualiseur, il est utile et pertinent sur un plan pédagogique de pouvoir partager la visualisation d\'une production « papier » d\'élèves et ainsi de mixer papier et numérique. ', 'ab6959f9-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6a2638-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b46ce0-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des solutions interactives : TBI, VPI, ou ENI ? (Tableau Blanc Interactif, Vidéoprojecteur Interactif, Ecran Numérique Interactif)', 'Les nouveaux outils numériques interactifs ont eu un impact fort dans le domaine de l\'éducation. Les présentations des enseignants sont de nos jours de plus en plus attractives et efficaces pour transmettre du savoir et obtenir une meilleure participation des élèves.', 'ab6a40fe-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6ae724-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b4aadd-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous équipé la direction de l\'école d\'un ordinateur, scanner, imprimante ?', 'Le bureau de la direction doit être équipé d\'un ensemble numérique permettant l\'accès et l\'utilisation des applications en ligne du ministère de l\'Éducation nationale, de la Jeunesse et des Sports (MENJS). La présence d\'un scanner est indispensable (si cette fonction n\'est pas présente sur le photocopieur).', 'ab6b00c8-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6ba8ff-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b4c02a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous établi un réseau informatique dans les écoles ?', 'Le socle numérique prévoit que chaque salle de classe dispose a minima de deux points d\'accès au réseau. Un de ces points servira à la connexion d\'une éventuelle classe mobile par le biais d\'une borne Wi-Fi. L\'autre point permettra la connexion du poste de travail relié au vidéoprojecteur.', 'ab6bc5ce-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6c7288-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b4d42f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab6d497a-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6df59a-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b4e83a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab6e0f63-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6eb489-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b4fc2e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de pointage (tablette, code-barres, borne) ?', 'La mise en place d\'outils de pointage dématérialisé (tablette, lecteur code-barres) vous permet de gagner du temps en évitant les doubles saisies. En lien direct avec votre progiciel de gestion vous éviterez les erreurs de ressaisie.', 'ab6ecea6-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab6f7420-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b50ed0-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab71106b-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab71b97c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b52212-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab71d449-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab727d0b-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b53592-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de pointage (tablette, code-barres, borne) ?', 'La mise en place d\'outils de pointage dématérialisé (tablette, lecteur code-barres) vous permet de gagner du temps en évitant les doubles saisies. En lien direct avec votre progiciel de gestion vous éviterez les erreurs de ressaisie.', 'ab729703-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab733bac-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b548b7-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion (réservation, facturation) ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab741aa6-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab750594-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b55c50-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab752112-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab75c49c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b577e4-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab76bdd0-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab77658d-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b591b7-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un logiciel de gestion de parc automobile?', 'Un logiciel de parc automobile permet de piloter avec précision la flotte de votre structure. Il permet d\'accéder à toutes les informations liées aux véhicules de vos collaborateurs et de maîtriser votre budget.', 'ab777f6d-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab7829a5-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b5a746-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une gestion de planning ?', 'La mise en place d\'un logiciel de gestion de planning permet de vous faire gagner du temps et de la visibilité sur vos équipes. Il permet également d\'offrir beaucoup plus de flexibilité à vos agents pour la saisie de leurs demandes. ', 'ab784323-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab78e96a-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b5beea-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un portail web ?', 'La mise en place d\'un portail Web permet au citoyen de s\'inscrire au service, de consulter et payer les factures et de déposer des pièces justificatives.', 'ab7902bb-cd45-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'ab79a951-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b5d48e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion et de facturation ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab7a8441-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7b4b7e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b5ec8c-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab7b65f2-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7c0f0e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b6025e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion des contrôles des assainissements non collectifs (ANC) ?', 'Un logiciel d\'assainissement non collectif gère l\'ensemble des modalités de contrôle des installations. Diagnostics, gestion des zonages, organisation des tournées des contrôleurs sont des exemples de possibilités offertes par ce type de logiciel.', 'ab7c2992-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7cd1d8-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b61622-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion et de facturation ?', 'La mise en place d\'un logiciel de gestion vous permet de gérer vos administrés, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches et de proposer à vos citoyens différents services.', 'ab7dacda-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7e5443-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b62a33-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion de bac ?', 'Un logiciel vous permettra de piloter la gestion technique des déchets, d\'adapter les paramètres des tournées et d\'optimiser les coûts de collecte et de traitement.', 'ab7e6db0-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7f1169-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b64114-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab7f2b73-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab7fd155-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b65808-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution SIG gérée en interne ?', 'La mise en place d\'une solution de SIG permet de recueillir, traiter, analyser et de présenter des données de type spatiales et géographiques. La dimension géographique de l\'information peut être une aide précieuse pour la prise de décision dans le domaine de l\'urbanisme, mise en place de réseaux (eaux, électricité...).', 'ab81de29-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab828685-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b66e11-cd79-11ed-b976-0cc47a39c2c0', 'Votre solution SIG est-elle accessible depuis le web via un navigateur ?', 'La mise en place d\'une solution SIG dite \"en ligne\" permet d\'avoir accès à un logiciel grâce à une simple connexion à Internet via n\'importe quel navigateur web et ainsi de faciliter les échanges avec vos différents partenaires.', 'ab82c6d8-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab836f89-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b682a1-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de gestion et de facturation dédiée au transport ?', 'La mise en place d\'une solution de gestion et facturation dédiée au transport permet de simplifier la facturation des usagers et  d\'avoir une vision globale pour optimiser vos ressources.  ', 'ab84304d-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab84bef7-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b69675-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'ab84dab1-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab85698b-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b6a9d1-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de gestion de parc ?', 'La mise en place d\'une solution de gestion de parc permet d\'avoir une vision globale de l\'acquisition, de l\'entretien, du renouvellement... et ainsi d\'optimiser vos ressources.  ', 'ab858318-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab8630d5-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b6bde6-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de gestion de planning de chauffeur ?', 'La mise en place d\'une solution de gestion de planning permet l\'optimisation de ceux-ci, un suivi de l\'activité et l\'adaptation de la charge de travail.', 'ab864a5b-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab86de67-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b6d0b6-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de gestion des dossiers d\'urbanisme (DP, DT, PC ...) ?', 'La mise en place d\'une solution de gestion des dossiers d\'urbanisme permet d\'avoir un suivi global de l\'instruction des dossiers et permet une fluidité dans les échanges avec les différents partenaires et ainsi de gagner du temps sur les délais d\'instruction.', 'ab86f914-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab87a329-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b6e47b-cd79-11ed-b976-0cc47a39c2c0', 'Votre Plan Local d\'Urbanisme (PLU) est-il disponible en ligne pour vos citoyens ?', 'Mettre à disposition la consultation du PLU pour vos administrés vous permet de leur proposer un service supplémentaire.', 'ab87bcca-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab8866bf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b6f985-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un portail web permettant au pétitionnaire de déposer une autorisation d\'urbanisme ?', 'La mise en place d\'un portail citoyen pour la dépose des autorisations d\'urbanisme permet aux administrés et aux instructeurs de gagner du temps.', 'ab888094-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab8928bf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4);
INSERT INTO `recommandation` (`Id`, `Titre`, `Text`, `IdQuestion`, `IdCategorie`, `IdReponse`, `NiveauReco`, `StatutReco`) VALUES
('34b70dd4-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la possibilité de signer les décisions d\'urbanisme via un parapheur électronique ?', 'Le parapheur électronique permet à l\'élu de signer les décisions d\'urbanisme en masse et pas nécessairement depuis la mairie', 'ab894246-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab8a2bfb-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b7232f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place une solution de Saisine par Voie Electronique ?', 'La saisine par voie électronique permet le dépôt des demandes d\'urbanisme par vos administrés par voie dématérialisée à tout moment et en tous lieux. C\'est une obligation concernant l\'urbanisme depuis le 1er janvier 2022. ', 'ab8a455b-cd45-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'ab8ad29e-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b737be-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous une solution informatique de gestion des contacts ?', 'Afin de fiabiliser et de sécuriser vos données entre chaque service en collectivité, une GRC (Gestion Relation Citoyen) facilitera vos traitements de données et donc, votre quotidien.', 'ab8aef0e-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8bcece-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b77d6e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous désigné un délégué à la protection des données (DPO) ?', 'Depuis le 25 mai 2018, l\'article 37 du RGPD précise que les organismes publics ont l\'obligation de désigner un délégué à la protection des données (DPO). Il a pour rôle de piloter la mise en conformité de l\'organisme notamment en exerçant les missions définies à l\'article 39 du RGPD.', 'ab8be8ed-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8cb3dd-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b79780-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous constitué votre registre des activités de traitement ?', 'L\'article 30 du RGPD dispose que \"\"\"\"chaque responsable du traitement et, le cas échéant, le représentant du responsable du traitement tiennent un registre des activités de traitement effectuées sous leur responsabilité.\"\"\"\"\r\nConcrètement, votre structure doit procéder au recensement des traitements à caractère personnel et détailler un certain nombre d\'informations. Il est conseillé de confier cette mission au délégué à la protection des données.', 'ab8cd19f-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8d943d-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b7b76e-cd79-11ed-b976-0cc47a39c2c0', 'Numérisez-vous les courriers entrants dans votre structure ?', 'La numérisation de votre courrier entrant (courriers, mails...) optimise la traçabilité de l\'information et fluidifie la circulation des documents entre vos collaborateurs.', 'ab8dae6d-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8e596e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b7d007-cd79-11ed-b976-0cc47a39c2c0', 'Disposez-vous d\'un outil de gestion des documents ?', 'La mise en place une GED (Gestion Electronique de Documents) permet le stockage, le classement et l\'archivage des documents sur une même plateforme. Un projet de GED améliore donc la recherche documentaire, la gestion et le suivi des documents durant tout leur cycle de vie.', 'ab8e737b-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8f1828-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b7e6fa-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous des outils pour le transfert de fichiers volumineux ?', 'L\'utilisation d\'un cloud ou d\'outils de transfert de fichiers sécurisés est indispensable pour échanger des fichiers volumineux.', 'ab8f31a4-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab8fd472-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b80146-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous fait un état des lieux de votre débit internet ?', 'Pour utiliser des outils collaboratifs et profiter pleinement de vos logiciels hébergés, il est nécessaire de disposer d\'une connexion internet rapide et stable.', 'ab8fee05-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab909048-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b81e4f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des bornes wifi à disposition du public dans vos bâtiments communaux ?', 'La mise en place de borne wifi public dans vos bâtiments communaux est un service à la population. Il est important cependant de protéger cet accès en le séparant de votre réseau privé, en filtrant les données utilisées sur ce réseau et en gardant une trace des logs d\'utilisation.', 'ab90a9fd-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab914d75-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b83dc4-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des bornes wifi public dans vos espaces communaux ouverts ?', 'La mise en place de bornes wifi public dans vos espaces communaux est un service à la population. Il est important cependant de protéger cet accès en le séparant de votre réseau privé, en filtrant les données utilisées sur ce réseau et en gardant une trace des logs d\'utilisation.', 'ab916714-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab920aec-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b8574b-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils informatiques en libre accès de type médiathèque ?', 'La mise en place d\'outils numériques à disposition dans les médiathèques fournit un service à vos administrés et contribue à réduire la fracture numérique.', 'ab922471-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab92cdb6-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b87960-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils informatiques en libre accès du type poste public à l\'accueil ?', 'La mise en place d\'outils numériques à disposition à votre accueil fournit un service à vos administrés et contribue à réduire la fracture numérique.', 'ab92e729-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab938d00-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b89260-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils informatiques d\'affichage numérique ?', 'L\'un des premiers avantages de l\'affichage public est sa visibilité. L\'affichage dynamique vous aide, entre autres, à augmenter votre visibilité et à toucher un public plus large.', 'ab93a628-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab9448cf-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b8a9ec-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils informatiques en libre accès du type borne d\'information ?', 'La Borne Services Publics, borne d\'accueil des usagers en mairie, est une solution interactive qui permet de réaliser les démarches simples en toute autonomie.', 'ab9461e7-cd45-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'ab9504fb-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b8c30e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion de comptabilité ?', 'Respectueux de la réglementation, un progiciel de comptabilité vous assure fiabilité et vous permet de dématérialiser vos échanges avec les plateformes dédiées comme Chorus.', 'ab951f7d-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab95c2c6-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b8db6c-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la dématérialisation des échanges avec votre comptable ?', 'La mise en place de la dématérialisation des échanges avec votre comptable nécessite un paramétrage de votre logiciel de finances ainsi qu\'une phase de test. Avec la généralisation des dépôts de factures sur chorus, cette mise en place est indispensable.', 'ab95dc09-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab968280-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b8f65a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la signature électronique des flux comptables à destination du trésor public ?', 'La mise en place d\'un certificat de signature électronique permet de sécuriser l\'échange de vos flux comptables avec le trésor public.', 'ab969c30-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab973eb4-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b90d8f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un parapheur électronique ?', 'La mise en place d\'un parapheur électronique permet de remplacer la signature \"papier\" en gardant la même valeur juridique à l\'heure où la dématérialisation est de plus en plus présente. Il s\'agit d\'un outil indispensable dans ces nouveaux usages.', 'ab9758b7-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9818bf-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b92764-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la validation des pièces justificatives via un parapheur électronique ?', 'La mise en place d\'un parapheur électronique permet de remplacer la signature \"papier\" en gardant la même valeur juridique à l\'heure où la dématérialisation est de plus en plus présente. Il s\'agit d\'un outil indispensable dans ces nouveaux usages.', 'ab9839ae-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab99061e-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b93f5f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un connecteur Chorus ?', 'Depuis le 1er janvier 2020, les émetteurs de factures à destination d\'entités publiques ont l\'obligation de les déposer sur le portail Chorus Pro. De votre côté, vous devez consulter ce portail, vérifier la présence de factures et mettre à jour le statut de mise en paiement de ces dernières. La mise en place d\'un connecteur Chorus permet de relier le portail à votre logiciel de finances et d\'automatiser la réception des factures et la mise à jour des statuts.', 'ab992690-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab99f659-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b955c8-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un connecteur GED en lien avec votre comptabilité ?', 'La mise en place d\'un connecteur permet le dépôt automatisé des PJ traitées en comptabilité vers une GED et ainsi permet de rendre les PJ traitées disponibles aux utilisateurs de la GED, ce depuis n\'importe quel endroit et sans accès au logiciel de finances.', 'ab9a3789-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9af3bb-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b96b48-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place le PES RETOUR entre votre logiciel de comptabilité et votre trésorerie ?', 'La mise en place du PES RETOUR permet d\'intégrer dans votre logiciel de comptabilité des paiements effectués et les recettes reçues par votre trésorier.', 'ab9b0e05-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9bc15f-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b98008-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la dématérialisation des budgets auprès du contrôle de légalité et de votre trésorerie ?', 'La mise en place d\'Actes budgétaires permet la transmission dématérialisée de vos budgets auprès de la sous-préfecture et de votre trésorier', 'ab9bdb9d-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9c87c4-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b998aa-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion de l\'état civil ?', 'La mise en place d\'un logiciel de gestion de l\'état civil permet le suivi de vos demandes liées à l\'état civil, de dématérialiser et sécuriser les échanges avec l\'INSEE, COMEDEC. ', 'ab9ca347-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9d1690-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34b9b37f-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la déclaration des décès en ligne via l\'application CertDC', 'La mise en place de la déclaration en ligne permet une obtention des causes du décès immédiate pour la veille et l\'alerte sanitaire, permet d\'alléger des charges liées au certificat papier et facilite l\'exploitation des données pour tous les acteurs.', 'ab9d331f-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9da062-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34b9d01e-cd79-11ed-b976-0cc47a39c2c0', ' Avez-vous mis en place une solution de Saisine par Voie Electronique ? ', 'La saisine par voie électronique permet à vos administrés de saisir votre collectivité à tout moment et en tous lieux. Cette obligation résulte du décret n°2016-1491 du 4 novembre 2016. ', 'ab9dbc4b-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9e5afd-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34b9e87b-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la démarche simplifiée pour vos démarches administratives ?', 'Demarches-simplifiees.fr est une application entièrement en ligne qui permet à tous les organismes assurant des missions de service public de créer des démarches en quelques minutes et de gérer les demandes des usagers sur une plateforme dédiée (source beta.gouv.fr).', 'ab9e75f6-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9f2a23-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba03b3-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous fait numériser vos actes d\'état civil ?', 'Faire numériser vos actes d\'état civil facilite la gestion courante des demandes de copie, extrait... Cela évite également la manipulation des registres et ainsi leur dégradation prématurée.', 'ab9f4866-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'ab9faa7b-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba1c39-cd79-11ed-b976-0cc47a39c2c0', 'Les actes numérisés sont-ils intégrés à votre solution logiciel d\'état civil ?', 'Intégrer les actes numérisés dans logiciel d\'état civil permet, par le biais d\'une opération d\'indexation, d\'optimiser les recherches d\'actes et de fournir rapidement les informations demandées par les usagers et autres instances.', 'ab9fc45c-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba02468-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba359e-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion des élections ?', 'La mise en place d\'un logiciel de gestion des élections permet la gestion de toutes les opérations électorales de l\'inscription à l\'organisation des scrutins avec une connexion avec le REU de l\'INSEE.', 'aba03e0a-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba09e4d-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba4e3c-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion du recensement citoyen ?', 'La mise en place d\'un logiciel de gestion de recensement citoyen permet la réalisation des démarches liées au service national et de procéder aux échanges dématérialisés avec le Bureau du Service National (BSN).', 'aba0ba4b-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba11ab3-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba64dd-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion du cimetière ?', 'La mise en place d\'un logiciel de gestion administrative du cimetière permet de simplifier les opérations liées aux concessions, emplacements, travaux... Les échanges avec les usagers et autres partenaires.', 'aba13464-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba1950d-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34ba7abf-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un plan de cimetière informatisé en lien avec votre logiciel de gestion ?', 'La mise en place d\'un plan informatisé en lien avec votre logiciel cimetière permet d\'accéder depuis le plan aux données administratives liées aux concessions, emplacements...', 'aba1b056-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba2105a-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34babe9a-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la dématérialisation des échanges Insee en lien avec l\'état civil ?', 'La mise en place de la liaison AIREPPNET ou SDFI avec l\'INSEE permet la dématérialisation des échanges avec l\'INSEE.', 'aba22a3d-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba28a4c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34bad4b6-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la dématérialisation des échanges PECOTO en lien avec le recensement citoyen ?', 'La mise en place des échanges des données de recensement citoyen avec votre centre du service national permet de procéder aux échanges dématérialisés avec votre centre du service national par le biais du site internet MaJdc (Presaje).', 'aba2a49a-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba303d6-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34baf395-cd79-11ed-b976-0cc47a39c2c0', 'Utilisez- vous votre logiciel de gestion élections pour effectuer le tirage au sort des jurés d\'assises ?', 'La mise en place d\'un tirage au sort à partir de votre logiciel de gestion élections facilite la procédure du tirage au sort jusqu\'au courrier de convocation des jurés.', 'aba31d91-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba37c4c-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34bb0b00-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place COMEDEC (COMmunication Electronique des Données de l\'État Civil) ? ', 'La mise en place du dispositif COMEDEC permet l\'échange dématérialisé et sécurisé des données de l\'état civil entre les différents partenaires tels que la préfecture, les notaires, les mairies...', 'aba395c1-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba3f5b1-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34bb1fbb-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous un logiciel de gestion des ressources humaines ?', 'La mise en place d\'un progiciel de gestion des ressources humaines vous permet d\'automatiser et de fiabiliser vos cycles de payes. Il permet également d\'assurer les échanges dématérialisés avec les plateformes de gestion de déclarations sociales.', 'aba4108f-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba4bdde-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34bb397b-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un portail agent (gestion des congés) ?', 'La mise en place d\'un portail Web accessible par les agents pour la gestion de leurs congés permet la dématérialisation des demandes d\'absences ainsi que la validation et la consultation des soldes et planning.', 'aba4d8fc-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba57cb7-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34bb52d2-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la DSN ?', 'Il s\'agit d\'une déclaration mensuelle obligatoire depuis le  1er janvier 2022 pour toutes tailles de collectivités. Sa mise en place nécessite un paramétrage de vos logiciels de paye et éventuellement la mise en place d\'un connecteur pour faciliter le dépôt mensuel et la lecture des Comptes Rendus Métier (CRM) reçus des différents organismes.', 'aba596d8-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba63b33-cd45-11ec-bd0e-0cc47a0ad120', 3, 4),
('34bb6a60-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place la dématérialisation des bulletins de paie ?', 'La remise des bulletins de paie aux agents de la collectivité via un coffre-fort numérique permet une démarche éco-responsable et d\'économie de coût sur le process papier. Ils sont accessibles à tout moment et en tout lieu par l\'agent qui n\'a plus à effectuer un stockage à son domicile, ce qui évite les pertes ou destruction suite à un sinistre par exemple. ', 'aba65523-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba6fcb8-cd45-11ec-bd0e-0cc47a0ad120', 1, 4),
('34bb7f8b-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un connecteur DSN pour récuperer l\'ensemble de vos déclarations automatiquement ?', 'La mise en place d\'un connecteur en lien avec le progiciel métier pour automatiser les envois et réceptions des déclaration mensuelles.', 'aba716e2-cd45-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'aba7bfc1-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34bb94b0-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion ?', 'La mise en place d\'outils de gestion permet de simplifier la gestion des biens, de l\'état des lieux en passant par la location et l\'entretien ainsi que la communication avec les résidents.', 'aba89f60-cd45-11ec-bd0e-0cc47a0ad120', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'aba939c5-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34bbada4-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place des outils de gestion et de facturation ?', 'La mise en place d\'un logiciel de gestion et de facturation vous permet de fiabiliser la facturation, les encaissements, de centraliser et de sécuriser vos informations. Il permet également d\'automatiser vos tâches.', 'abaa002a-cd45-11ec-bd0e-0cc47a0ad120', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'abaa7a59-cd45-11ec-bd0e-0cc47a0ad120', 2, 4),
('34bbb973-cd79-11ed-b976-0cc47a39c2c0', 'Avez-vous mis en place un système de paiement en ligne ?', 'La mise en place d\'un moyen de paiement en ligne permet à vos administrés de payer directement leurs factures. Votre trésorerie pourra vous conseiller sur les outils à mettre en place.', 'abaa9406-cd45-11ec-bd0e-0cc47a0ad120', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'abab0ae9-cd45-11ec-bd0e-0cc47a0ad120', 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ref_niveaureco`
--

DROP TABLE IF EXISTS `ref_niveaureco`;
CREATE TABLE IF NOT EXISTS `ref_niveaureco` (
  `Id` int NOT NULL,
  `Label` varchar(50) NOT NULL,
  `Couleur` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `ref_niveaureco`
--

INSERT INTO `ref_niveaureco` (`Id`, `Label`, `Couleur`) VALUES
(0, 'Facultatif', '#EEFFE5'),
(1, 'Recommandé', '#FFF5E5'),
(2, 'Fortement recommandé', '#F2D8AD'),
(3, 'Obligatoire', '#FFE5E5');

-- --------------------------------------------------------

--
-- Structure de la table `ref_statutreco`
--

DROP TABLE IF EXISTS `ref_statutreco`;
CREATE TABLE IF NOT EXISTS `ref_statutreco` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Label` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `ref_statutreco`
--

INSERT INTO `ref_statutreco` (`Id`, `Label`) VALUES
(0, 'Planifiée'),
(1, 'En cours'),
(2, 'Réalisée'),
(3, 'À planifier'),
(4, 'À définir');

-- --------------------------------------------------------

--
-- Structure de la table `ref_typecollectivite`
--

DROP TABLE IF EXISTS `ref_typecollectivite`;
CREATE TABLE IF NOT EXISTS `ref_typecollectivite` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `ref_typecollectivite`
--

INSERT INTO `ref_typecollectivite` (`Id`, `Nom`) VALUES
('3e85465a-ffff-11eb-acf0-0cc47a0ad120', 'CA'),
('57482110-fe97-11eb-acf0-0cc47a0ad120', 'Mairie'),
('5748268d-fe97-11eb-acf0-0cc47a0ad120', 'COMCOM'),
('8f9d4a91-fff3-11eb-acf0-0cc47a0ad120', 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

DROP TABLE IF EXISTS `reponse`;
CREATE TABLE IF NOT EXISTS `reponse` (
  `Id` char(36) NOT NULL,
  `IdQuestion` char(36) NOT NULL,
  `Ponderation` int NOT NULL,
  `Text` longtext,
  `Type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `QuestionId` (`IdQuestion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `reponse`
--

INSERT INTO `reponse` (`Id`, `IdQuestion`, `Ponderation`, `Text`, `Type`) VALUES
('ab3ba5ff-cd45-11ec-bd0e-0cc47a0ad120', 'ab3aa246-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab3bbb43-cd45-11ec-bd0e-0cc47a0ad120', 'ab3aa246-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3c446c-cd45-11ec-bd0e-0cc47a0ad120', 'ab3bcfb5-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab3c58f9-cd45-11ec-bd0e-0cc47a0ad120', 'ab3bcfb5-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3ce36b-cd45-11ec-bd0e-0cc47a0ad120', 'ab3c6d7e-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab3cf810-cd45-11ec-bd0e-0cc47a0ad120', 'ab3c6d7e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3d84a8-cd45-11ec-bd0e-0cc47a0ad120', 'ab3d0cf2-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab3d995c-cd45-11ec-bd0e-0cc47a0ad120', 'ab3d0cf2-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3e2678-cd45-11ec-bd0e-0cc47a0ad120', 'ab3daf28-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab3e3b22-cd45-11ec-bd0e-0cc47a0ad120', 'ab3daf28-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3ed2d9-cd45-11ec-bd0e-0cc47a0ad120', 'ab3e4ea7-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab3eeb0f-cd45-11ec-bd0e-0cc47a0ad120', 'ab3e4ea7-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab3f9376-cd45-11ec-bd0e-0cc47a0ad120', 'ab3f04b8-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab3fabba-cd45-11ec-bd0e-0cc47a0ad120', 'ab3f04b8-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab405650-cd45-11ec-bd0e-0cc47a0ad120', 'ab3fc5d7-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab406e5f-cd45-11ec-bd0e-0cc47a0ad120', 'ab3fc5d7-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab40ebe1-cd45-11ec-bd0e-0cc47a0ad120', 'ab408974-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab41017c-cd45-11ec-bd0e-0cc47a0ad120', 'ab408974-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab41ab59-cd45-11ec-bd0e-0cc47a0ad120', 'ab411b09-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab41c155-cd45-11ec-bd0e-0cc47a0ad120', 'ab411b09-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab426dbe-cd45-11ec-bd0e-0cc47a0ad120', 'ab41dce1-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab4283d3-cd45-11ec-bd0e-0cc47a0ad120', 'ab41dce1-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4329b9-cd45-11ec-bd0e-0cc47a0ad120', 'ab429d50-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab433f58-cd45-11ec-bd0e-0cc47a0ad120', 'ab429d50-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab43eede-cd45-11ec-bd0e-0cc47a0ad120', 'ab435bd2-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4404cf-cd45-11ec-bd0e-0cc47a0ad120', 'ab435bd2-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab44af12-cd45-11ec-bd0e-0cc47a0ad120', 'ab44208b-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab44c49e-cd45-11ec-bd0e-0cc47a0ad120', 'ab44208b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab456e23-cd45-11ec-bd0e-0cc47a0ad120', 'ab44de34-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab45855c-cd45-11ec-bd0e-0cc47a0ad120', 'ab44de34-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab46370e-cd45-11ec-bd0e-0cc47a0ad120', 'ab45a29b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab464e9a-cd45-11ec-bd0e-0cc47a0ad120', 'ab45a29b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab46ff4b-cd45-11ec-bd0e-0cc47a0ad120', 'ab466b2c-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab471651-cd45-11ec-bd0e-0cc47a0ad120', 'ab466b2c-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab47c69e-cd45-11ec-bd0e-0cc47a0ad120', 'ab472fb0-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab47dcba-cd45-11ec-bd0e-0cc47a0ad120', 'ab472fb0-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab48c3af-cd45-11ec-bd0e-0cc47a0ad120', 'ab47f73f-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab48da1e-cd45-11ec-bd0e-0cc47a0ad120', 'ab47f73f-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab498cee-cd45-11ec-bd0e-0cc47a0ad120', 'ab48f405-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab49a2d3-cd45-11ec-bd0e-0cc47a0ad120', 'ab48f405-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4a5232-cd45-11ec-bd0e-0cc47a0ad120', 'ab49bc5b-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab4a67c9-cd45-11ec-bd0e-0cc47a0ad120', 'ab49bc5b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4b1090-cd45-11ec-bd0e-0cc47a0ad120', 'ab4a81dc-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab4b261a-cd45-11ec-bd0e-0cc47a0ad120', 'ab4a81dc-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4bcd9d-cd45-11ec-bd0e-0cc47a0ad120', 'ab4b3f51-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4be333-cd45-11ec-bd0e-0cc47a0ad120', 'ab4b3f51-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4c94fb-cd45-11ec-bd0e-0cc47a0ad120', 'ab4c039e-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4caacf-cd45-11ec-bd0e-0cc47a0ad120', 'ab4c039e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4d5427-cd45-11ec-bd0e-0cc47a0ad120', 'ab4cc48f-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4d69d2-cd45-11ec-bd0e-0cc47a0ad120', 'ab4cc48f-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4e3282-cd45-11ec-bd0e-0cc47a0ad120', 'ab4d831e-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4e4905-cd45-11ec-bd0e-0cc47a0ad120', 'ab4d831e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4ef48d-cd45-11ec-bd0e-0cc47a0ad120', 'ab4e6414-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab4f0ab0-cd45-11ec-bd0e-0cc47a0ad120', 'ab4e6414-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab4fb26f-cd45-11ec-bd0e-0cc47a0ad120', 'ab4f252a-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab4fc84c-cd45-11ec-bd0e-0cc47a0ad120', 'ab4f252a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5059ef-cd45-11ec-bd0e-0cc47a0ad120', 'ab4fe1b6-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab50706e-cd45-11ec-bd0e-0cc47a0ad120', 'ab4fe1b6-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab50b848-cd45-11ec-bd0e-0cc47a0ad120', 'ab508a12-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab50cddc-cd45-11ec-bd0e-0cc47a0ad120', 'ab508a12-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab51780b-cd45-11ec-bd0e-0cc47a0ad120', 'ab50e978-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab518dee-cd45-11ec-bd0e-0cc47a0ad120', 'ab50e978-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab52350a-cd45-11ec-bd0e-0cc47a0ad120', 'ab51a728-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab524b61-cd45-11ec-bd0e-0cc47a0ad120', 'ab51a728-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab52934c-cd45-11ec-bd0e-0cc47a0ad120', 'ab52653d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab52a90f-cd45-11ec-bd0e-0cc47a0ad120', 'ab52653d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab534c87-cd45-11ec-bd0e-0cc47a0ad120', 'ab52c214-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab5361cf-cd45-11ec-bd0e-0cc47a0ad120', 'ab52c214-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab53ab4b-cd45-11ec-bd0e-0cc47a0ad120', 'ab537b68-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab53c0ef-cd45-11ec-bd0e-0cc47a0ad120', 'ab537b68-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab549352-cd45-11ec-bd0e-0cc47a0ad120', 'ab53fe3b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab54a9a6-cd45-11ec-bd0e-0cc47a0ad120', 'ab53fe3b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5557e9-cd45-11ec-bd0e-0cc47a0ad120', 'ab54c31b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab556e0e-cd45-11ec-bd0e-0cc47a0ad120', 'ab54c31b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab56193d-cd45-11ec-bd0e-0cc47a0ad120', 'ab5587b9-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab562fcd-cd45-11ec-bd0e-0cc47a0ad120', 'ab5587b9-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab57082b-cd45-11ec-bd0e-0cc47a0ad120', 'ab5649c0-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab571f8e-cd45-11ec-bd0e-0cc47a0ad120', 'ab5649c0-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab57cc58-cd45-11ec-bd0e-0cc47a0ad120', 'ab5739f5-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab57e35a-cd45-11ec-bd0e-0cc47a0ad120', 'ab5739f5-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab58931e-cd45-11ec-bd0e-0cc47a0ad120', 'ab57fd8e-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab58a9fd-cd45-11ec-bd0e-0cc47a0ad120', 'ab57fd8e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5954a2-cd45-11ec-bd0e-0cc47a0ad120', 'ab58c456-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab596ab1-cd45-11ec-bd0e-0cc47a0ad120', 'ab58c456-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5a13af-cd45-11ec-bd0e-0cc47a0ad120', 'ab5983fb-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5a29cb-cd45-11ec-bd0e-0cc47a0ad120', 'ab5983fb-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5adf1f-cd45-11ec-bd0e-0cc47a0ad120', 'ab5a4305-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5af4c2-cd45-11ec-bd0e-0cc47a0ad120', 'ab5a4305-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5b9eac-cd45-11ec-bd0e-0cc47a0ad120', 'ab5b0e90-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5bb49e-cd45-11ec-bd0e-0cc47a0ad120', 'ab5b0e90-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5c5dd1-cd45-11ec-bd0e-0cc47a0ad120', 'ab5bce94-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5c7363-cd45-11ec-bd0e-0cc47a0ad120', 'ab5bce94-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5d1951-cd45-11ec-bd0e-0cc47a0ad120', 'ab5c8c89-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab5d2ee3-cd45-11ec-bd0e-0cc47a0ad120', 'ab5c8c89-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5dd5b8-cd45-11ec-bd0e-0cc47a0ad120', 'ab5d4871-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab5deb38-cd45-11ec-bd0e-0cc47a0ad120', 'ab5d4871-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5e9295-cd45-11ec-bd0e-0cc47a0ad120', 'ab5e046c-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5ea84d-cd45-11ec-bd0e-0cc47a0ad120', 'ab5e046c-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab5f4f03-cd45-11ec-bd0e-0cc47a0ad120', 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab5f647e-cd45-11ec-bd0e-0cc47a0ad120', 'ab5ec19b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab600d6b-cd45-11ec-bd0e-0cc47a0ad120', 'ab5f7e22-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab60235a-cd45-11ec-bd0e-0cc47a0ad120', 'ab5f7e22-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab60d22a-cd45-11ec-bd0e-0cc47a0ad120', 'ab603d11-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab60e856-cd45-11ec-bd0e-0cc47a0ad120', 'ab603d11-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab61fd36-cd45-11ec-bd0e-0cc47a0ad120', 'ab6102c3-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab621384-cd45-11ec-bd0e-0cc47a0ad120', 'ab6102c3-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab62c18e-cd45-11ec-bd0e-0cc47a0ad120', 'ab622f61-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab62d8b8-cd45-11ec-bd0e-0cc47a0ad120', 'ab622f61-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6388f1-cd45-11ec-bd0e-0cc47a0ad120', 'ab62f221-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab639f8f-cd45-11ec-bd0e-0cc47a0ad120', 'ab62f221-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6447ca-cd45-11ec-bd0e-0cc47a0ad120', 'ab63ba83-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab645e59-cd45-11ec-bd0e-0cc47a0ad120', 'ab63ba83-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6507c0-cd45-11ec-bd0e-0cc47a0ad120', 'ab64778c-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab651e0c-cd45-11ec-bd0e-0cc47a0ad120', 'ab64778c-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab65c81e-cd45-11ec-bd0e-0cc47a0ad120', 'ab6539c5-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab65dd72-cd45-11ec-bd0e-0cc47a0ad120', 'ab6539c5-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab66b519-cd45-11ec-bd0e-0cc47a0ad120', 'ab65f8b2-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab66cd73-cd45-11ec-bd0e-0cc47a0ad120', 'ab65f8b2-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab677dd9-cd45-11ec-bd0e-0cc47a0ad120', 'ab66e93a-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab67942d-cd45-11ec-bd0e-0cc47a0ad120', 'ab66e93a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab684020-cd45-11ec-bd0e-0cc47a0ad120', 'ab67adf8-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab68565d-cd45-11ec-bd0e-0cc47a0ad120', 'ab67adf8-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6928cd-cd45-11ec-bd0e-0cc47a0ad120', 'ab68933e-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab693f8b-cd45-11ec-bd0e-0cc47a0ad120', 'ab68933e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6a0fcb-cd45-11ec-bd0e-0cc47a0ad120', 'ab6959f9-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab6a2638-cd45-11ec-bd0e-0cc47a0ad120', 'ab6959f9-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6ad190-cd45-11ec-bd0e-0cc47a0ad120', 'ab6a40fe-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab6ae724-cd45-11ec-bd0e-0cc47a0ad120', 'ab6a40fe-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6b9173-cd45-11ec-bd0e-0cc47a0ad120', 'ab6b00c8-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab6ba8ff-cd45-11ec-bd0e-0cc47a0ad120', 'ab6b00c8-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6c5c35-cd45-11ec-bd0e-0cc47a0ad120', 'ab6bc5ce-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab6c7288-cd45-11ec-bd0e-0cc47a0ad120', 'ab6bc5ce-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6d1b46-cd45-11ec-bd0e-0cc47a0ad120', 'ab6c8d84-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab6d3050-cd45-11ec-bd0e-0cc47a0ad120', 'ab6c8d84-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6ddf83-cd45-11ec-bd0e-0cc47a0ad120', 'ab6d497a-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab6df59a-cd45-11ec-bd0e-0cc47a0ad120', 'ab6d497a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6e9dac-cd45-11ec-bd0e-0cc47a0ad120', 'ab6e0f63-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab6eb489-cd45-11ec-bd0e-0cc47a0ad120', 'ab6e0f63-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab6f5e18-cd45-11ec-bd0e-0cc47a0ad120', 'ab6ecea6-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab6f7420-cd45-11ec-bd0e-0cc47a0ad120', 'ab6ecea6-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab701f85-cd45-11ec-bd0e-0cc47a0ad120', 'ab6f8e67-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab70353e-cd45-11ec-bd0e-0cc47a0ad120', 'ab6f8e67-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab71a378-cd45-11ec-bd0e-0cc47a0ad120', 'ab71106b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab71b97c-cd45-11ec-bd0e-0cc47a0ad120', 'ab71106b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab72678b-cd45-11ec-bd0e-0cc47a0ad120', 'ab71d449-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab727d0b-cd45-11ec-bd0e-0cc47a0ad120', 'ab71d449-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab73258d-cd45-11ec-bd0e-0cc47a0ad120', 'ab729703-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab733bac-cd45-11ec-bd0e-0cc47a0ad120', 'ab729703-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab73e7f0-cd45-11ec-bd0e-0cc47a0ad120', 'ab73559a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab74004e-cd45-11ec-bd0e-0cc47a0ad120', 'ab73559a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab74ef7a-cd45-11ec-bd0e-0cc47a0ad120', 'ab741aa6-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab750594-cd45-11ec-bd0e-0cc47a0ad120', 'ab741aa6-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab75ae23-cd45-11ec-bd0e-0cc47a0ad120', 'ab752112-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab75c49c-cd45-11ec-bd0e-0cc47a0ad120', 'ab752112-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab768f5f-cd45-11ec-bd0e-0cc47a0ad120', 'ab75dde3-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab76a4a5-cd45-11ec-bd0e-0cc47a0ad120', 'ab75dde3-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab774f85-cd45-11ec-bd0e-0cc47a0ad120', 'ab76bdd0-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab77658d-cd45-11ec-bd0e-0cc47a0ad120', 'ab76bdd0-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7813e2-cd45-11ec-bd0e-0cc47a0ad120', 'ab777f6d-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7829a5-cd45-11ec-bd0e-0cc47a0ad120', 'ab777f6d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab78d3d8-cd45-11ec-bd0e-0cc47a0ad120', 'ab784323-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab78e96a-cd45-11ec-bd0e-0cc47a0ad120', 'ab784323-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab799177-cd45-11ec-bd0e-0cc47a0ad120', 'ab7902bb-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab79a951-cd45-11ec-bd0e-0cc47a0ad120', 'ab7902bb-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7a526a-cd45-11ec-bd0e-0cc47a0ad120', 'ab79c351-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab7a6a97-cd45-11ec-bd0e-0cc47a0ad120', 'ab79c351-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7b3572-cd45-11ec-bd0e-0cc47a0ad120', 'ab7a8441-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7b4b7e-cd45-11ec-bd0e-0cc47a0ad120', 'ab7a8441-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7bf943-cd45-11ec-bd0e-0cc47a0ad120', 'ab7b65f2-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7c0f0e-cd45-11ec-bd0e-0cc47a0ad120', 'ab7b65f2-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7cbb76-cd45-11ec-bd0e-0cc47a0ad120', 'ab7c2992-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7cd1d8-cd45-11ec-bd0e-0cc47a0ad120', 'ab7c2992-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7d7dc4-cd45-11ec-bd0e-0cc47a0ad120', 'ab7cec01-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab7d9398-cd45-11ec-bd0e-0cc47a0ad120', 'ab7cec01-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7e3e1f-cd45-11ec-bd0e-0cc47a0ad120', 'ab7dacda-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7e5443-cd45-11ec-bd0e-0cc47a0ad120', 'ab7dacda-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7efb0e-cd45-11ec-bd0e-0cc47a0ad120', 'ab7e6db0-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7f1169-cd45-11ec-bd0e-0cc47a0ad120', 'ab7e6db0-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab7fbbd0-cd45-11ec-bd0e-0cc47a0ad120', 'ab7f2b73-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab7fd155-cd45-11ec-bd0e-0cc47a0ad120', 'ab7f2b73-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab81aef6-cd45-11ec-bd0e-0cc47a0ad120', 'ab7feb54-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab81c494-cd45-11ec-bd0e-0cc47a0ad120', 'ab7feb54-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab82700a-cd45-11ec-bd0e-0cc47a0ad120', 'ab81de29-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab828685-cd45-11ec-bd0e-0cc47a0ad120', 'ab81de29-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8359d3-cd45-11ec-bd0e-0cc47a0ad120', 'ab82c6d8-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab836f89-cd45-11ec-bd0e-0cc47a0ad120', 'ab82c6d8-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab83fe8f-cd45-11ec-bd0e-0cc47a0ad120', 'ab838904-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('ab84150d-cd45-11ec-bd0e-0cc47a0ad120', 'ab838904-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab84a927-cd45-11ec-bd0e-0cc47a0ad120', 'ab84304d-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab84bef7-cd45-11ec-bd0e-0cc47a0ad120', 'ab84304d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8551a9-cd45-11ec-bd0e-0cc47a0ad120', 'ab84dab1-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab85698b-cd45-11ec-bd0e-0cc47a0ad120', 'ab84dab1-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab85f7f6-cd45-11ec-bd0e-0cc47a0ad120', 'ab858318-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8630d5-cd45-11ec-bd0e-0cc47a0ad120', 'ab858318-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab86c7a1-cd45-11ec-bd0e-0cc47a0ad120', 'ab864a5b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab86de67-cd45-11ec-bd0e-0cc47a0ad120', 'ab864a5b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab878d16-cd45-11ec-bd0e-0cc47a0ad120', 'ab86f914-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab87a329-cd45-11ec-bd0e-0cc47a0ad120', 'ab86f914-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8850f5-cd45-11ec-bd0e-0cc47a0ad120', 'ab87bcca-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8866bf-cd45-11ec-bd0e-0cc47a0ad120', 'ab87bcca-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab891303-cd45-11ec-bd0e-0cc47a0ad120', 'ab888094-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8928bf-cd45-11ec-bd0e-0cc47a0ad120', 'ab888094-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8a1634-cd45-11ec-bd0e-0cc47a0ad120', 'ab894246-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8a2bfb-cd45-11ec-bd0e-0cc47a0ad120', 'ab894246-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8abc96-cd45-11ec-bd0e-0cc47a0ad120', 'ab8a455b-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab8ad29e-cd45-11ec-bd0e-0cc47a0ad120', 'ab8a455b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8bb731-cd45-11ec-bd0e-0cc47a0ad120', 'ab8aef0e-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab8bcece-cd45-11ec-bd0e-0cc47a0ad120', 'ab8aef0e-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8c9b3b-cd45-11ec-bd0e-0cc47a0ad120', 'ab8be8ed-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab8cb3dd-cd45-11ec-bd0e-0cc47a0ad120', 'ab8be8ed-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8d7a98-cd45-11ec-bd0e-0cc47a0ad120', 'ab8cd19f-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab8d943d-cd45-11ec-bd0e-0cc47a0ad120', 'ab8cd19f-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8e4352-cd45-11ec-bd0e-0cc47a0ad120', 'ab8dae6d-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8e596e-cd45-11ec-bd0e-0cc47a0ad120', 'ab8dae6d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8f0199-cd45-11ec-bd0e-0cc47a0ad120', 'ab8e737b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8f1828-cd45-11ec-bd0e-0cc47a0ad120', 'ab8e737b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab8fbeed-cd45-11ec-bd0e-0cc47a0ad120', 'ab8f31a4-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab8fd472-cd45-11ec-bd0e-0cc47a0ad120', 'ab8f31a4-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab907ace-cd45-11ec-bd0e-0cc47a0ad120', 'ab8fee05-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab909048-cd45-11ec-bd0e-0cc47a0ad120', 'ab8fee05-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab913758-cd45-11ec-bd0e-0cc47a0ad120', 'ab90a9fd-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab914d75-cd45-11ec-bd0e-0cc47a0ad120', 'ab90a9fd-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab91f4e2-cd45-11ec-bd0e-0cc47a0ad120', 'ab916714-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab920aec-cd45-11ec-bd0e-0cc47a0ad120', 'ab916714-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab92b81b-cd45-11ec-bd0e-0cc47a0ad120', 'ab922471-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab92cdb6-cd45-11ec-bd0e-0cc47a0ad120', 'ab922471-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab93776f-cd45-11ec-bd0e-0cc47a0ad120', 'ab92e729-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab938d00-cd45-11ec-bd0e-0cc47a0ad120', 'ab92e729-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab943210-cd45-11ec-bd0e-0cc47a0ad120', 'ab93a628-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9448cf-cd45-11ec-bd0e-0cc47a0ad120', 'ab93a628-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab94eeee-cd45-11ec-bd0e-0cc47a0ad120', 'ab9461e7-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9504fb-cd45-11ec-bd0e-0cc47a0ad120', 'ab9461e7-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab95ad4a-cd45-11ec-bd0e-0cc47a0ad120', 'ab951f7d-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab95c2c6-cd45-11ec-bd0e-0cc47a0ad120', 'ab951f7d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab966bd1-cd45-11ec-bd0e-0cc47a0ad120', 'ab95dc09-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab968280-cd45-11ec-bd0e-0cc47a0ad120', 'ab95dc09-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab97287b-cd45-11ec-bd0e-0cc47a0ad120', 'ab969c30-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab973eb4-cd45-11ec-bd0e-0cc47a0ad120', 'ab969c30-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab980188-cd45-11ec-bd0e-0cc47a0ad120', 'ab9758b7-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab9818bf-cd45-11ec-bd0e-0cc47a0ad120', 'ab9758b7-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab98eb7c-cd45-11ec-bd0e-0cc47a0ad120', 'ab9839ae-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab99061e-cd45-11ec-bd0e-0cc47a0ad120', 'ab9839ae-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab99dd08-cd45-11ec-bd0e-0cc47a0ad120', 'ab992690-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab99f659-cd45-11ec-bd0e-0cc47a0ad120', 'ab992690-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9adc89-cd45-11ec-bd0e-0cc47a0ad120', 'ab9a3789-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9af3bb-cd45-11ec-bd0e-0cc47a0ad120', 'ab9a3789-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9baa01-cd45-11ec-bd0e-0cc47a0ad120', 'ab9b0e05-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9bc15f-cd45-11ec-bd0e-0cc47a0ad120', 'ab9b0e05-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9c70cd-cd45-11ec-bd0e-0cc47a0ad120', 'ab9bdb9d-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab9c87c4-cd45-11ec-bd0e-0cc47a0ad120', 'ab9bdb9d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9cfbe2-cd45-11ec-bd0e-0cc47a0ad120', 'ab9ca347-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('ab9d1690-cd45-11ec-bd0e-0cc47a0ad120', 'ab9ca347-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9d87d5-cd45-11ec-bd0e-0cc47a0ad120', 'ab9d331f-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9da062-cd45-11ec-bd0e-0cc47a0ad120', 'ab9d331f-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9e40dc-cd45-11ec-bd0e-0cc47a0ad120', 'ab9dbc4b-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('ab9e5afd-cd45-11ec-bd0e-0cc47a0ad120', 'ab9dbc4b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9f11ee-cd45-11ec-bd0e-0cc47a0ad120', 'ab9e75f6-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9f2a23-cd45-11ec-bd0e-0cc47a0ad120', 'ab9e75f6-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('ab9f947b-cd45-11ec-bd0e-0cc47a0ad120', 'ab9f4866-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('ab9faa7b-cd45-11ec-bd0e-0cc47a0ad120', 'ab9f4866-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba00e9d-cd45-11ec-bd0e-0cc47a0ad120', 'ab9fc45c-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba02468-cd45-11ec-bd0e-0cc47a0ad120', 'ab9fc45c-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba086e0-cd45-11ec-bd0e-0cc47a0ad120', 'aba03e0a-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba09e4d-cd45-11ec-bd0e-0cc47a0ad120', 'aba03e0a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba1045d-cd45-11ec-bd0e-0cc47a0ad120', 'aba0ba4b-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba11ab3-cd45-11ec-bd0e-0cc47a0ad120', 'aba0ba4b-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba17ec1-cd45-11ec-bd0e-0cc47a0ad120', 'aba13464-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba1950d-cd45-11ec-bd0e-0cc47a0ad120', 'aba13464-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba1fa96-cd45-11ec-bd0e-0cc47a0ad120', 'aba1b056-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba2105a-cd45-11ec-bd0e-0cc47a0ad120', 'aba1b056-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba271e4-cd45-11ec-bd0e-0cc47a0ad120', 'aba22a3d-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba28a4c-cd45-11ec-bd0e-0cc47a0ad120', 'aba22a3d-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba2ee22-cd45-11ec-bd0e-0cc47a0ad120', 'aba2a49a-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba303d6-cd45-11ec-bd0e-0cc47a0ad120', 'aba2a49a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba36683-cd45-11ec-bd0e-0cc47a0ad120', 'aba31d91-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba37c4c-cd45-11ec-bd0e-0cc47a0ad120', 'aba31d91-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba3de8f-cd45-11ec-bd0e-0cc47a0ad120', 'aba395c1-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba3f5b1-cd45-11ec-bd0e-0cc47a0ad120', 'aba395c1-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba4a7f3-cd45-11ec-bd0e-0cc47a0ad120', 'aba4108f-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('aba4bdde-cd45-11ec-bd0e-0cc47a0ad120', 'aba4108f-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba56724-cd45-11ec-bd0e-0cc47a0ad120', 'aba4d8fc-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba57cb7-cd45-11ec-bd0e-0cc47a0ad120', 'aba4d8fc-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba6254e-cd45-11ec-bd0e-0cc47a0ad120', 'aba596d8-cd45-11ec-bd0e-0cc47a0ad120', 3, 'Oui', 'button'),
('aba63b33-cd45-11ec-bd0e-0cc47a0ad120', 'aba596d8-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba6e6fc-cd45-11ec-bd0e-0cc47a0ad120', 'aba65523-cd45-11ec-bd0e-0cc47a0ad120', 1, 'Oui', 'button'),
('aba6fcb8-cd45-11ec-bd0e-0cc47a0ad120', 'aba65523-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba7a7f2-cd45-11ec-bd0e-0cc47a0ad120', 'aba716e2-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('aba7bfc1-cd45-11ec-bd0e-0cc47a0ad120', 'aba716e2-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba84ed7-cd45-11ec-bd0e-0cc47a0ad120', 'aba7d939-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('aba864f5-cd45-11ec-bd0e-0cc47a0ad120', 'aba7d939-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba92398-cd45-11ec-bd0e-0cc47a0ad120', 'aba89f60-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('aba939c5-cd45-11ec-bd0e-0cc47a0ad120', 'aba89f60-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('aba9c4f7-cd45-11ec-bd0e-0cc47a0ad120', 'aba95415-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Oui', 'button'),
('aba9db92-cd45-11ec-bd0e-0cc47a0ad120', 'aba95415-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('abaa641b-cd45-11ec-bd0e-0cc47a0ad120', 'abaa002a-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('abaa7a59-cd45-11ec-bd0e-0cc47a0ad120', 'abaa002a-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button'),
('abaaf622-cd45-11ec-bd0e-0cc47a0ad120', 'abaa9406-cd45-11ec-bd0e-0cc47a0ad120', 2, 'Oui', 'button'),
('abab0ae9-cd45-11ec-bd0e-0cc47a0ad120', 'abaa9406-cd45-11ec-bd0e-0cc47a0ad120', 0, 'Non', 'button');

-- --------------------------------------------------------

--
-- Structure de la table `siret_temporaire`
--

DROP TABLE IF EXISTS `siret_temporaire`;
CREATE TABLE IF NOT EXISTS `siret_temporaire` (
  `Siret` char(14) NOT NULL,
  `Nom` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`Siret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE IF NOT EXISTS `theme` (
  `Id` char(36) NOT NULL,
  `IdCategorie` char(36) NOT NULL,
  `Theme` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `ThemeId` (`IdCategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`Id`, `IdCategorie`, `Theme`) VALUES
('9c975984-cd39-11ec-bd0e-0cc47a0ad120', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 'Délibérations'),
('9c97fdd6-cd39-11ec-bd0e-0cc47a0ad120', '8f5ca892-ec48-4ef0-aec3-0d13c5f96f99', 'Marchés publics'),
('9c98cea5-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Gazette locale'),
('9c991f1e-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Messagerie'),
('9c9a32df-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Outils collaboratifs'),
('9c9ab4a4-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Visuel'),
('9c9ce8fb-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Web'),
('9c9d882e-cd39-11ec-bd0e-0cc47a0ad120', '226ba507-f9d2-11eb-acf0-0cc47a0ad120', 'Outils complémentaires'),
('9c9e25b2-cd39-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'Arts'),
('9c9e899a-cd39-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'Bibliothèque ou médiathèque'),
('9c9f2669-cd39-11ec-bd0e-0cc47a0ad120', '44e20f60-f9d2-11eb-acf0-0cc47a0ad120', 'Musée & piscine'),
('9c9fc5f0-cd39-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'Maintenance'),
('9ca407db-cd39-11ec-bd0e-0cc47a0ad120', '743bda71-f9d2-11eb-acf0-0cc47a0ad120', 'Risque'),
('9ca58ebf-cd39-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'Équipements numériques'),
('9ca6e3e3-cd39-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'Périscolaire et restauration scolaire'),
('9ca96cc1-cd39-11ec-bd0e-0cc47a0ad120', 'bbee662b-f9d2-11eb-acf0-0cc47a0ad120', 'Petite enfance'),
('9caa40b5-cd39-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Gestion de l\'eau'),
('9cab38e8-cd39-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Gestion des déchets'),
('9cabe3b2-cd39-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Système d\'Information Géographique'),
('9cacff10-cd39-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Transport'),
('9cae2407-cd39-11ec-bd0e-0cc47a0ad120', 'ec564248-f9d2-11eb-acf0-0cc47a0ad120', 'Urbanisme'),
('9caec6db-cd39-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'Données personnelles'),
('9caf29a2-cd39-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'Gestion documentaire'),
('9cb00270-cd39-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'Internet et réseaux'),
('9cb0d711-cd39-11ec-bd0e-0cc47a0ad120', '0776d9ff-f9d3-11eb-acf0-0cc47a0ad120', 'Public'),
('9cb2f1af-cd39-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'Gestion financière'),
('9cb694e4-cd39-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'Relation citoyenne'),
('9cb7dc07-cd39-11ec-bd0e-0cc47a0ad120', '1cf6e204-f9d3-11eb-acf0-0cc47a0ad120', 'Ressources humaines'),
('9cb85204-cd39-11ec-bd0e-0cc47a0ad120', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'Logement'),
('9cb90a5b-cd39-11ec-bd0e-0cc47a0ad120', '3a942ae6-f9d3-11eb-acf0-0cc47a0ad120', 'Personnes âgées');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prenom` varchar(50) DEFAULT NULL,
  `Identifiant` varchar(300) NOT NULL,
  `Mail` varchar(200) DEFAULT NULL,
  `MotDePasse` varchar(200) DEFAULT NULL,
  `CollectiviteId` char(36) DEFAULT NULL,
  `Actif` tinyint(1) NOT NULL,
  `Admin` tinyint(1) NOT NULL,
  `Token` varchar(500) DEFAULT NULL,
  `IdMotDePasseOublie` char(36) DEFAULT NULL,
  `DateMotDePasseOublie` datetime DEFAULT NULL,
  `CGU` tinyint(1) NOT NULL DEFAULT '0',
  `IsVerifie` tinyint(1) NOT NULL DEFAULT '0',
  `SuperAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `SuperAdmin2` tinyint(1) NOT NULL DEFAULT '0',
  `OPSNId` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CollectiviteId` (`CollectiviteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`Id`, `Nom`, `Prenom`, `Identifiant`, `Mail`, `MotDePasse`, `CollectiviteId`, `Actif`, `Admin`, `Token`, `IdMotDePasseOublie`, `DateMotDePasseOublie`, `CGU`, `IsVerifie`, `SuperAdmin`, `SuperAdmin2`, `OPSNId`) VALUES
('366e0825-da33-11ee-a042-3822e20d3cc2', 'demo', 'demo', 'demo.demo', 'demo@demo.fr', '6095d5bbb845b8bac4c71a3448296fa9', '404', 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, '404');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurreponse`
--

DROP TABLE IF EXISTS `utilisateurreponse`;
CREATE TABLE IF NOT EXISTS `utilisateurreponse` (
  `Id` char(36) NOT NULL,
  `CollectiviteId` char(36) NOT NULL,
  `IdReponse` char(36) NOT NULL,
  `IdQuestion` char(36) NOT NULL,
  `InputText` longtext,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `CollectiviteId` (`CollectiviteId`),
  KEY `IdReponse` (`IdReponse`),
  KEY `IdQuestion` (`IdQuestion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurstatut`
--

DROP TABLE IF EXISTS `utilisateurstatut`;
CREATE TABLE IF NOT EXISTS `utilisateurstatut` (
  `Id` char(36) NOT NULL,
  `RecommandationId` char(36) NOT NULL,
  `UtilisateurId` char(36) NOT NULL,
  `StatutCode` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RecommandationId_2` (`RecommandationId`,`UtilisateurId`),
  KEY `RecommandationId` (`RecommandationId`),
  KEY `UtilisateurId` (`UtilisateurId`),
  KEY `StatutCode` (`StatutCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
